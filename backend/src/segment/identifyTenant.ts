import { Edition } from '@crowd/types'
import { SEGMENT_CONFIG, API_CONFIG } from '../conf'
import { client } from '@/utils/posthog'

export default async function identifyTenant(req) {

  try {
    posthogIdentifyTenant(req)
  } catch (error) {
    console.error(error)
  }

  if (SEGMENT_CONFIG.writeKey) {
    const Analytics = require('analytics-node')
    const analytics = new Analytics(SEGMENT_CONFIG.writeKey)

    if (API_CONFIG.edition === Edition.CROWD_HOSTED || API_CONFIG.edition === Edition.LFX) {
      if (!req.currentUser.email.includes('help@crowd.dev')) {
        analytics.group({
          userId: req.currentUser.id,
          groupId: req.currentTenant.id,
          traits:
            req.currentTenant.name !== 'temporaryName'
              ? {
                  name: req.currentTenant.name,
                }
              : undefined,
        })
      }
    } else if (API_CONFIG.edition === Edition.COMMUNITY) {
      if (!req.currentUser.email.includes('crowd.dev')) {
        analytics.group({
          userId: req.currentUser.id,
          groupId: req.currentTenant.id,
          traits: {
            createdAt: req.currentTenant.createdAt,
          },
        })
      }
    }
  }
}

export async function posthogIdentifyTenant(req) {
  client?.groupIdentify({
    groupType: 'tenant',
    groupKey: req.currentTenant.id,
    properties: {
      createdAt: req.currentTenant.createdAt,
      name: `${req.currentTenant.name} (${req.currentTenant.id})`,
    }
  })
}