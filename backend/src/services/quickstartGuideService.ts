import lodash from 'lodash'
import { LoggerBase } from '@crowd/logging'
import { IServiceOptions } from './IServiceOptions'
import {
  DEFAULT_GUIDES,
  QuickstartGuideMap,
  QuickstartGuideSettings,
  QuickstartGuideType,
} from '../types/quickstartGuideTypes'
import TenantUserRepository from '../database/repositories/tenantUserRepository'
import SettingsRepository from '@/database/repositories/settingsRepository'

export default class QuickstartGuideService extends LoggerBase {
  options: IServiceOptions

  constructor(options: IServiceOptions) {
    super(options.log)
    this.options = options
  }

  async updateSettings(settings: any) {
    const quickstartGuideSettings: QuickstartGuideSettings = lodash.pick(settings, [
      'isEagleEyeGuideDismissed',
      'isQuickstartGuideDismissed',
    ])

    const tenantUser = await TenantUserRepository.updateSettings(
      this.options.currentUser.id,
      quickstartGuideSettings,
      this.options,
    )

    return tenantUser
  }

  async find(): Promise<QuickstartGuideMap> {
    const guides: QuickstartGuideMap = JSON.parse(JSON.stringify(DEFAULT_GUIDES))
    const tenantSettings = await SettingsRepository.getTenantSettings(
      this.options.currentTenant.id,
      this.options,
    )

    if (QuickstartGuideType.EXPLORE_COLLEAGUES in guides) {
      guides[QuickstartGuideType.EXPLORE_COLLEAGUES].completed = tenantSettings.colleaguesViewed
    }

    if (QuickstartGuideType.EXPLORE_INTEGRATIONS in guides) {
      guides[QuickstartGuideType.EXPLORE_INTEGRATIONS].completed = tenantSettings.integrationsViewed
    }

    if (QuickstartGuideType.EXPLORE_ORGANIZATIONS in guides) {
      guides[QuickstartGuideType.EXPLORE_ORGANIZATIONS].completed =
        tenantSettings.organizationsViewed
    }

    if (QuickstartGuideType.EXPLORE_CONTACTS in guides) {
      guides[QuickstartGuideType.EXPLORE_CONTACTS].completed = tenantSettings.contactsViewed
    }

    if (QuickstartGuideType.VIEW_REPORT in guides) {
      guides[QuickstartGuideType.VIEW_REPORT].completed = tenantSettings.reportsViewed
    }

    if (QuickstartGuideType.EXPLORE_AUTOMATIONS in guides) {
      guides[QuickstartGuideType.EXPLORE_AUTOMATIONS].completed = tenantSettings.automationsViewed
    }

    return guides
  }
}
