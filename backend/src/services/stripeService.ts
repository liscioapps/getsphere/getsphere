import { Stripe } from 'stripe'
import { TenantPlans } from '@crowd/types'
import { PLANS_CONFIG } from '@/conf'

const stripe = new Stripe(PLANS_CONFIG.stripeSecretKey, {
  apiVersion: '2022-08-01',
  typescript: true,
})

class StripeService {
  static async retreiveSession(sessionId: string) {
    return stripe.checkout.sessions.retrieve(sessionId)
  }

  static async retreiveSubscription(subscriptionId: string) {
    return stripe.subscriptions.retrieve(subscriptionId)
  }

  static async createPortalSession(customerId: string) {
    return stripe.billingPortal.sessions.create({
      customer: customerId,
    })
  }

  static async updateCustomerMetadata(id: string, metadata: Record<string, any>) {
    return stripe.customers.update(id, {
      metadata,
    })
  }

  static async getOrCreateCustomer({
    email,
    id,
    tenantId,
  }: {
    email: string
    id: string
    tenantId: string
  }) {
    const customers = await stripe.customers.list({
      email,
    })
    if (customers.data.length) {
      return customers.data[0]
    }
    return stripe.customers.create({
      email,
      metadata: {
        userId: id,
        tenantId,
      },
    })
  }

  static async createTrialSubscription({
    user,
    priceId,
    trialDays,
  }: {
    user: { email: string; id: string; tenantId: string }
    priceId: string
    trialDays: number
  }) {
    const customer = await this.getOrCreateCustomer(user)
    return stripe.subscriptions.create({
      customer: customer.id,
      items: [{ price: priceId }],
      trial_period_days: trialDays,
    })
  }

  static getPlanFromProductId(productId: string) {
    if (productId === PLANS_CONFIG.stripeScalePlanProductId) {
      return TenantPlans.Business
    }
    if (productId === PLANS_CONFIG.stripeEssentialPlanProductId) {
      return TenantPlans.Starter
    }
    if (productId === PLANS_CONFIG.stripeEagleEyePlanProductId) {
      return TenantPlans.EagleEye
    }
    if (productId === PLANS_CONFIG.stripeGrowthPlanProductId) {
      return TenantPlans.Growth
    }
    if (productId === PLANS_CONFIG.stripeBasicPlanProductId) {
      return TenantPlans.Basic
    }
    return null
  }
}

export default StripeService
