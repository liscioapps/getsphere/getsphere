import { LoggerBase } from '@crowd/logging'
import { Resend } from 'resend'
import Handlebars from 'handlebars'
import fs from 'node:fs/promises'

export enum EmailTemplate {
  Invitation = 'invitation',
  PasswordReset = 'passwordReset',
  WeeklyAnalytics = 'weeklyAnalytics',
  IntegrationDone = 'integrationDone',
  CsvExport = 'csvExport',
  EmailAddressVerification = 'emailAddressVerification',
}

const getTemplate = async (template: EmailTemplate) => {
  switch (template) {
    case EmailTemplate.Invitation:
      return fs.readFile(process.cwd() + '/src/templates/mail/user-invite-template.hbs', 'utf8')
    case EmailTemplate.PasswordReset:
      // eslint-disable-next-line prefer-template
      return fs.readFile(process.cwd() + '/src/templates/mail/reset-password-template.hbs', 'utf8')
    case EmailTemplate.WeeklyAnalytics:
      throw new Error('Not implemented')
    case EmailTemplate.IntegrationDone:
      throw new Error('Not implemented')
    case EmailTemplate.CsvExport:
      throw new Error('Not implemented')
    case EmailTemplate.EmailAddressVerification:
      // eslint-disable-next-line prefer-template
      return fs.readFile(process.cwd() + '/src/templates/mail/verify-email-template.hbs', 'utf8')
    default:
      throw new Error(`Template ${template} not recognized`)
  }
}

const getSubjectLine = (template: EmailTemplate) => {
  switch (template) {
    case EmailTemplate.Invitation:
      return 'Welcome to Sphere'
    case EmailTemplate.PasswordReset:
      return 'Sphere Password Reset'
    case EmailTemplate.WeeklyAnalytics:
      throw new Error('Not implemented')
    case EmailTemplate.IntegrationDone:
      throw new Error('Not implemented')
    case EmailTemplate.CsvExport:
      throw new Error('Not implemented')
    case EmailTemplate.EmailAddressVerification:
      return 'Sphere - Please verify your email address'
    default:
      throw new Error(`Template ${template} not recognized`)
  }
}

const getHandlebars = async () => {
  if (!Handlebars.partials.base_email) {
    const base = await fs.readFile(
      // eslint-disable-next-line prefer-template
      process.cwd() + '/src/templates/mail/partials/base-email.hbs',
      'utf8',
    )
    Handlebars.registerPartial('base_email', base)
  }
  return Handlebars
}

type EmailInfo =
  | { tempate: EmailTemplate.Invitation; data: { tenant: string; link: string } }
  | { tempate: EmailTemplate.CsvExport; data: { link: string } }
  | { tempate: EmailTemplate.EmailAddressVerification; data: { link: string } }
  | { tempate: EmailTemplate.PasswordReset; data: { link: string } }

export default class EmailSender extends LoggerBase {
  private readonly resend: Resend

  constructor(private readonly info: EmailInfo) {
    super()
    this.resend = new Resend(process.env.RESEND_API_KEY as string)
    this.log.info('EmailSender created', info)
  }

  static get isConfigured(): boolean {
    return !!process.env.RESEND_API_KEY
  }

  async send({ email }: { email: string }): Promise<any> {
    this.log.info('Sending email', email)
    try {
      const handlebars = await getHandlebars()
      const template = await getTemplate(this.info.tempate)
      const body = handlebars.compile<EmailInfo['data']>(template)

      const res = await this.resend.emails.send({
        from: process.env.MAIL_FROM_ADDRESS as string,
        to: email,
        subject: getSubjectLine(this.info.tempate),
        html: body(this.info.data),
      })
      this.log.info('Email response', res)
    } catch (error) {
      this.log.error(error, 'Error sending email.')
      throw error
    }
  }
}
