export enum UnleashContextField {
  TENANT_ID = 'tenantId',
  PLAN = 'plan',
  AUTOMATION_COUNT = 'automationCount',
  AVAILABLE_INTEGRATIONS = 'availableIntegrations',
  MEMBER_ENRICHMENT_COUNT = 'memberEnrichmentCount',
  CSV_EXPORT_COUNT = 'csvExportCount',
}
