alter table public."settings"
    drop column if exists "reportsViewed",
    drop column if exists "automationsViewed",
    drop column if exists "colleaguesViewed",
    drop column if exists "integrationsViewed";