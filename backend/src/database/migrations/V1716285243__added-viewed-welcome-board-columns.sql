alter table public."settings"
    add COLUMN "reportsViewed" bool,
    add COLUMN "automationsViewed" bool,
    add COLUMN "colleaguesViewed" bool,
    add COLUMN "integrationsViewed" bool;
    