UPDATE tenants SET plan = 'Starter' WHERE plan = 'Essential';
ALTER TABLE tenants ALTER column plan SET DEFAULT 'Starter';