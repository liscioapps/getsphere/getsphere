import { DEFAULT_MEMBER_ATTRIBUTES } from '@crowd/integrations'
import TenantService from '../../../services/tenantService'
import getUserContext from '../../utils/getUserContext'
import MemberAttributeSettingsService from '../../../services/memberAttributeSettingsService'

/* eslint-disable no-console */

const addSynRemoteToMemberAttributes = async () => {
  const tenants = await TenantService._findAndCountAllForEveryUser({})
  const syncRemoteAttribute = DEFAULT_MEMBER_ATTRIBUTES.find((a) => a.name === 'syncRemote')

  // for each tenant
  for (const tenant of tenants.rows) {
    const userContext = await getUserContext(tenant.id)
    const mas = new MemberAttributeSettingsService(userContext)

    // check already exists
    const attrs = await mas.findAndCountAll({ filter: { name: syncRemoteAttribute.name } })

    if (attrs.count === 0) {
      console.log(`Creating syncRemote member attribute for tenant ${tenant.id}`)
      await mas.create({
        name: syncRemoteAttribute.name,
        label: syncRemoteAttribute.label,
        type: syncRemoteAttribute.type,
        canDelete: syncRemoteAttribute.canDelete,
        show: syncRemoteAttribute.show,
      })
    }
  }
}

addSynRemoteToMemberAttributes()
