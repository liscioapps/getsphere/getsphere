import { PostHog } from 'posthog-node'

export const client: PostHog | undefined = process.env.CROWD_POSTHOG_KEY
  ? new PostHog(process.env.CROWD_POSTHOG_KEY, { host: 'https://us.i.posthog.com' })
  : undefined

export async function recordEvent(id: string, event: string) {
  client.capture({
    distinctId: id,
    event,
  })
}
