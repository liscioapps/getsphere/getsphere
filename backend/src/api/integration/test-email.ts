import EmailSender, { EmailTemplate } from '@/services/emailSender'
import Permissions from '../../security/permissions'
import track from '../../segment/track'
import IntegrationService from '../../services/integrationService'
import PermissionChecker from '../../services/user/permissionChecker'

// /**
//  * POST /tenant/{tenantId}/integration
//  * @summary Create or update an integration
//  * @tag Activities
//  * @security Bearer
//  * @description Create or update an integration. Existence is checked by sourceId and tenantId.
//  * @pathParam {string} tenantId - Your workspace/tenant ID
//  * @bodyContent {IntegrationUpsertInput} application/json
//  * @response 200 - Ok
//  * @responseContent {Integration} 200.application/json
//  * @responseExample {IntegrationUpsert} 200.application/json.Integration
//  * @response 401 - Unauthorized
//  * @response 404 - Not found
//  * @response 429 - Too many requests
//  */
export default async (req, res) => {
  console.log("sending2...")
  await new EmailSender({ tempate: EmailTemplate.EmailAddressVerification, data: { link: "https://google.com" } }).send({ email: "joshuaguenther@gmail.com" });
  return res.status(200).json("sent!");
}
