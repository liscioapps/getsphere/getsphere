import AuthService from '../../services/auth/authService'

export default async (req, res) => {
  const payload = await AuthService.impersonate(
    req.body.userId,
    req.body.key,
    req,
  )

  await req.responseHandler.success(req, res, payload)
}
