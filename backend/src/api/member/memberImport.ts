import track from '@/segment/telemetryTrack'
import Permissions from '../../security/permissions'
import MemberService from '../../services/memberService'
import PermissionChecker from '../../services/user/permissionChecker'

export default async (req, res) => {
  new PermissionChecker(req).validateHas(Permissions.values.memberImport)

  const payload = await new MemberService(req).import(req.body)

  track('Member Imported', { ...req.body }, { ...req })

  await req.responseHandler.success(req, res, payload)
}
