import TenantService from "@/services/tenantService"


export default async (req, res) => {
    const url = await new TenantService(req).createStripePortalLink()

    await req.responseHandler.success(req, res, url)
}
