import { Error403 } from '@crowd/common'
import TenantService from '../../services/tenantService'
import identifyTenant from '@/segment/identifyTenant'

export default async (req, res) => {
  if (!req.currentUser || !req.currentUser.id) {
    throw new Error403(req.language)
  }

  const payload = await new TenantService(req).makeOnboarded(req.params.id)
  identifyTenant({ ...req, currentTenant: payload })

  await req.responseHandler.success(req, res, payload)
}
