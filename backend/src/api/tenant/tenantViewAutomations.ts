import TenantService from '../../services/tenantService'

export default async (req, res) => {
  const payload = await new TenantService(req).viewAutomations()

  await req.responseHandler.success(req, res, payload)
}
