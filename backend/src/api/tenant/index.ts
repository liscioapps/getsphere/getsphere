import { safeWrap } from '../../middlewares/errorMiddleware'

export default (app) => {
  app.post(
    `/tenant/invitation/:token/accept`,
    safeWrap(require('./tenantInvitationAccept').default),
  )
  app.delete(
    `/tenant/invitation/:token/decline`,
    safeWrap(require('./tenantInvitationDecline').default),
  )
  app.post(`/tenant`, safeWrap(require('./tenantCreate').default))
  app.put(`/tenant/:id`, safeWrap(require('./tenantUpdate').default))
  app.delete(`/tenant`, safeWrap(require('./tenantDestroy').default))
  app.get(`/tenant`, safeWrap(require('./tenantList').default))
  app.get(`/tenant/url`, safeWrap(require('./tenantFind').default))
  app.get(`/tenant/:id`, safeWrap(require('./tenantFind').default))
  app.get(`/tenant/:id/name`, safeWrap(require('./tenantFindName').default))
  app.get(`/tenant/:tenantId/membersToMerge`, safeWrap(require('./tenantMembersToMerge').default))
  app.get(
    `/tenant/:tenantId/organizationsToMerge`,
    safeWrap(require('./tenantOrganizationsToMerge').default),
  )
  app.post(`/tenant/:tenantId/sampleData`, safeWrap(require('./tenantGenerateSampleData').default))
  app.delete(`/tenant/:tenantId/sampleData`, safeWrap(require('./tenantDeleteSampleData').default))
  app.post(
    `/tenant/:tenantId/viewOrganizations`,
    safeWrap(require('./tenantViewOrganizations').default),
  )
  app.post(`/tenant/:tenantId/viewContacts`, safeWrap(require('./tenantViewContacts').default))
  app.post(
    `/tenant/:tenantId/payment/confirm`,
    safeWrap(require('./tenantPaymentConfirmation').default),
  )
  app.post(
    `/tenant/:tenantId/viewReports`,
    safeWrap(require('./tenantViewReports').default),
  )
  app.post(
    `/tenant/:tenantId/viewAutomations`,
    safeWrap(require('./tenantViewAutomations').default),
  )
  app.post(
    `/tenant/:tenantId/viewColleagues`,
    safeWrap(require('./tenantViewColleagues').default),
  )
  app.post(
    `/tenant/:tenantId/viewIntegrations`,
    safeWrap(require('./tenantViewIntegrations').default),
  )
  app.post(
    `/tenant/:tenantId/markAllViewed`,
    safeWrap(require('./tenantMarkAllViewed').default),
  )
  app.post(
    `/tenant/:tenantId/getStripeCustomerPortalLink`,
    safeWrap(require('./tenantGetStripeCustomerPortalLink').default),
  )
  app.put(`/tenant/:id/makeOnboarded`, safeWrap(require('./tenantMakeOnboarded').default))
}
