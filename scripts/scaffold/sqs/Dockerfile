FROM openjdk:8-jre

ARG ELASTICMQ_VERSION
RUN apt-get update && apt-get install -y curl ca-certificates wget
RUN mkdir -p /opt/elasticmq/log /opt/elasticmq/lib /opt/elasticmq/config

ENV ELASTICMQ_VERSION ${ELASTICMQ_VERSION:-1.6.6}
RUN curl -sfLo /opt/elasticmq/lib/elasticmq.jar https://s3-eu-west-1.amazonaws.com/softwaremill-public/elasticmq-server-${ELASTICMQ_VERSION}.jar


WORKDIR /opt/elasticmq

ENV H2_VERSION 2.2.224
RUN wget https://github.com/h2database/h2database/releases/download/version-${H2_VERSION}/h2-${H2_VERSION}.jar

COPY queue.conf /opt/elasticmq/conf/elasticmq.conf
COPY docker-entrypoint.sh /docker-entrypoint.sh

RUN chmod +x /docker-entrypoint.sh

EXPOSE 9324
EXPOSE 9325
EXPOSE 9092

ENTRYPOINT [ "/docker-entrypoint.sh" ]
