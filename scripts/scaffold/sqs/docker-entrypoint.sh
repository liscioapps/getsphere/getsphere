#!/usr/bin/env bash

set -m

java -Dconfig.file=/opt/elasticmq/conf/elasticmq.conf -jar /opt/elasticmq/lib/elasticmq.jar &
java -jar h2-2.2.224.jar -webAllowOthers -tcpAllowOthers -baseDir /data &

wait -n

exit $?
