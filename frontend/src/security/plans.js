/**
 * List of Plans
 */
class Plans {
  static get values() {
    return {
      starter: 'Starter',
      growth: 'Growth',
      enterprise: 'Enterprise',
      business: 'Business',
      eagleEye: 'Eagle Eye',
      basic: 'Basic',
      none: 'none',
      trialExpired: 'trialExpired',
    };
  }

  static get communityValues() {
    return {
      community: 'Community',
      custom: 'Custom',
    };
  }
}

export default Plans;
