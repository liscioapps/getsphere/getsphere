import Plans from '@/security/plans';

export const planLimits = {
  enrichment: {
    [Plans.values.starter]: 0,
    [Plans.values.growth]: 1000,
    [Plans.values.business]: 'unlimited',
    [Plans.values.enterprise]: 'unlimited',
  },
  export: {
    [Plans.values.starter]: 2,
    [Plans.values.growth]: 10,
    [Plans.values.business]: 20,
    [Plans.values.enterprise]: 'unlimited',
  },
  automation: {
    [Plans.values.starter]: 2,
    [Plans.values.growth]: 10,
    [Plans.values.business]: 20,
    [Plans.values.enterprise]: 'unlimited',
  },
  integrations: {
    [Plans.values.basic]: true,
    [Plans.values.starter]: 'unlimited',
    [Plans.values.growth]: 'unlimited',
    [Plans.values.business]: 'unlimited',
    [Plans.values.enterprise]: 'unlimited',
  },
};
