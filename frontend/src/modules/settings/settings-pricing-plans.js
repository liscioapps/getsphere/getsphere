import Plans from '@/security/plans';
import config from '@/config';
import { renderCal } from '@/utils/cals';

const hostedPlans = Plans.values;
const communityPlans = Plans.communityValues;

const intoToCrowdDevCal = ({
  displayCalDialog,
}) => {
  displayCalDialog();
  setTimeout(() => {
    renderCal({
      calLink: 'team/CrowdDotDev/sales',
    });
  }, 0);
};

const customPlanCal = ({
  displayCalDialog,
}) => {
  window.open(config.enterprisePlanLink, '_blank');
};

export const openCustomerPortalLink = ({ customerPortalLink }) => {
  window.open(customerPortalLink ?? config.stripe.customerPortalLink, '_blank');
};

const getLink = ({ monthlyPayment, nonCCTrial, plan }) => {
  const planLinks = config.stripe[plan];
  const periodLinks = monthlyPayment ? planLinks?.monthly : planLinks?.yearly;
  return nonCCTrial ? periodLinks?.nonCC : periodLinks?.default;
};

const openPlan = (config) => {
  window.open(getLink(config) ?? '', '_self');
};

/**
 * ctaLabel: Copy shown in the CTA dependent on the active plan.
 * Key of ctaLabel represents the active plan, value represents the copy that should appear on the corresponding column plan
 * ctaAction: Action triggered by CTA click dependent on the active plan.
 * Key of ctaAction represents the acttive plan, value represents the set of actions trigerred when the corresponding column plan button is clicked.
 */
export const plans = {
  hosted: [
    {
      key: hostedPlans.basic,
      title: 'Basic',
      description: 'Choose any Starter integration, and add on additional integrations for $25/each.',
      price: '$25/month',
      priceMonthly: '$35/month',
      features: [
        {
          includes: true,
          value: '1 user',
        },
        {
          includes: true,
          value: '500 monthly active contacts',
        },
        {
          includes: true,
          value: '1 integration',
        },
        {
          includes: true,
          value: 'Add additional integrations',
        },
        {
          includes: true,
          value: 'Full API access & data export',
        },
      ],
      ctaLabel: {
        [Plans.values.none]: 'Start 14-day free trial',
        [Plans.values.trialExpired]: 'Manage Subscription',
        [Plans.values.basic]: null,
        [Plans.values.starter]: 'Downgrade to Basic',
        [Plans.values.eagleEye]: 'Downgrade to Basic',
        [Plans.values.growth]: 'Downgrade to Basic',
        [Plans.values.business]: 'Downgrade to Basic',
        [Plans.values.enterprise]: 'Downgrade to Basic',
      },
      ctaAction: {
        [Plans.values.none]: (config) => {
          openPlan({ plan: 'basic', ...config });
        },
        [Plans.values.trialExpired]: openCustomerPortalLink,
        [Plans.values.basic]: openCustomerPortalLink,
        [Plans.values.starter]: openCustomerPortalLink,
        [Plans.values.eagleEye]: openCustomerPortalLink,
        [Plans.values.growth]: openCustomerPortalLink,
        [Plans.values.business]: openCustomerPortalLink,
        [Plans.values.enterprise]: openCustomerPortalLink,
      },
    },
    {
      key: hostedPlans.starter,
      title: 'Starter',
      description: 'Unify & act on your developer data',
      price: '$150/month',
      priceMonthly: '$175/month',
      features: [
        {
          includes: true,
          value: '1 - 3 users',
        },
        {
          includes: true,
          value: '2k monthly active contacts',
        },
        {
          includes: true,
          value: '2 automations',
        },
        {
          includes: true,
          value: 'Default reporting',
        },
        {
          includes: true,
          value: 'Full API access & data export',
        },
        {
          includes: true,
          value: 'Integrates with',
          integrations: ['github', 'discord', 'slack', 'discourse', 'devto', 'hackernews', 'reddit'],
        },
      ],
      ctaLabel: {
        [Plans.values.none]: 'Start 14-day free trial',
        [Plans.values.trialExpired]: 'Manage Subscription',
        [Plans.values.basic]: 'Upgrade to Starter',
        [Plans.values.starter]: null,
        [Plans.values.eagleEye]: 'Downgrade to Starter',
        [Plans.values.growth]: 'Downgrade to Starter',
        [Plans.values.business]: 'Downgrade to Starter',
        [Plans.values.enterprise]: 'Downgrade to Starter',
      },
      ctaAction: {
        [Plans.values.none]: (config) => {
          openPlan({ plan: 'starter', ...config });
        },
        [Plans.values.trialExpired]: openCustomerPortalLink,
        [Plans.values.basic]: openCustomerPortalLink,
        [Plans.values.starter]: openCustomerPortalLink,
        [Plans.values.eagleEye]: openCustomerPortalLink,
        [Plans.values.growth]: openCustomerPortalLink,
        [Plans.values.business]: openCustomerPortalLink,
        [Plans.values.enterprise]: customPlanCal,
      },
    },
    {
      key: hostedPlans.business,
      title: 'Business',
      description:
        'Bring social insights to your data',
      price: '$450/month',
      priceMonthly: '$525/month',
      features: [
        {
          includes: true,
          value: 'Unlimited users',
        },
        {
          includes: true,
          value: '10k monthly active contacts',
        },
        {
          includes: true,
          value: '20 active automation workflows',
        },
        {
          includes: true,
          value: 'Default & custom reports',
        },
        {
          includes: true,
          value: 'Full API access & data export',
        },
        {
          includes: true,
          value: 'Starter integrations plus:',
          integrations: ['linkedin', 'twitter', 'zapier', 'n8n'],
        },
      ],
      ctaLabel: {
        [Plans.values.basic]: 'Upgrade to Business',
        [Plans.values.trialExpired]: 'Manage Subscription',
        [Plans.values.none]: 'Start 14-day free trial',
        [Plans.values.eagleEye]: 'Upgrade to Business',
        [Plans.values.starter]: 'Upgrade to Business',
        [Plans.values.growth]: 'Upgrade to Business',
        [Plans.values.enterprise]: 'Downgrade to Business',
      },
      ctaAction: {
        [Plans.values.none]: (config) => {
          openPlan({ plan: 'business', ...config });
        },
        [Plans.values.basic]: openCustomerPortalLink,
        [Plans.values.none]: openCustomerPortalLink,
        [Plans.values.eagleEye]: openCustomerPortalLink,
        [Plans.values.starter]: openCustomerPortalLink,
        [Plans.values.growth]: openCustomerPortalLink,
        [Plans.values.enterprise]: openCustomerPortalLink,
      },
    },
    {
      key: hostedPlans.enterprise,
      title: 'Enterprise',
      description:
        'Tailored to your needs',
      price: 'Custom',
      features: [
        {
          includes: true,
          value: 'Unlimited users',
        },
        {
          includes: true,
          value: 'Unlimited contacts',
        },
        {
          includes: true,
          value: 'Unlimited automations',
        },
        {
          includes: true,
          value: 'Default & custom reporting',
        },
        {
          includes: true,
          value: 'Full API access & data export',
        },
        {
          includes: true,
          value: 'Business integrations plus:',
          integrations: ['salesforce', 'hubspot'],
        },
      ],
      ctaLabel: {
        [Plans.values.basic]: 'Book a call',
        [Plans.values.trialExpired]: 'Book a call',
        [Plans.values.none]: 'Book a call',
        [Plans.values.eagleEye]: 'Book a call',
        [Plans.values.starter]: 'Book a call',
        [Plans.values.growth]: 'Book a call',
        [Plans.values.business]: 'Book a call',
      },
      ctaAction: {
        [Plans.values.none]: customPlanCal,
        [Plans.values.trialExpired]: customPlanCal,
        [Plans.values.eagleEye]: customPlanCal,
        [Plans.values.starter]: customPlanCal,
        [Plans.values.growth]: customPlanCal,
        [Plans.values.business]: customPlanCal,
      },
    },
  ],
  community: [
    {
      key: communityPlans.community,
      title: 'Community',
      description:
        "Keep ownership of your data and host GetSphere.dev's community version for free on your own premises",
      price: 'Free',
      features: [
        'Unlimited users',
        'Unlimited community contacts & activities',
        'Community management',
        'Community intelligence',
        'Integrations with GitHub, Discord, Slack, X/Twitter, DEV, Hacker News',
        'Community support',
      ],
      ctaLabel: {
        [Plans.communityValues.custom]: 'Downgrage to Community',
      },
      ctaAction: {
        [Plans.communityValues.custom]: openCustomerPortalLink,
      },
    },
    {
      key: communityPlans.custom,
      title: 'Custom',
      description:
        "Get access to GetSphere.dev's premium features and support, and host the platform on your own premises",
      price: 'On request',
      featuresNote: 'Everything in Community, plus:',
      features: [
        'Community growth',
        'Organization-level insights',
        'Custom integrations',
        'Enterprise-grade support',
        'LinkedIn integration',
        'Unlimited contact enrichments (automated)',
      ],
      ctaLabel: {
        [Plans.communityValues.community]: 'Book a call',
      },
      ctaAction: {
        [Plans.communityValues.community]: customPlanCal,
      },
    },
  ],
};
