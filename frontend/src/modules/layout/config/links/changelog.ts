import { MenuLink } from '@/modules/layout/types/MenuLink';

const changelog: MenuLink = {
  id: 'changelog',
  label: 'Changelog',
  icon: 'ri-megaphone-line',
  href: 'https://docs.getsphere.dev/technical-docs/changelog',
  display: () => true,
  disable: () => false,
};

export default changelog;
