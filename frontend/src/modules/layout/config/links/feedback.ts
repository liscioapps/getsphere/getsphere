import { MenuLink } from '@/modules/layout/types/MenuLink';

const feedback: MenuLink = {
  id: 'feedback',
  label: 'Feedback',
  icon: 'ri-feedback-line',
  href: 'https://insigh.to/b/getsphere',
  display: () => true,
  disable: () => false,
};

export default feedback;
