import { isCurrentDateAfterGivenWorkingDays } from '@/utils/date';
import {
  ERROR_BANNER_WORKING_DAYS_DISPLAY,
} from '@/modules/integration/integration-store';
import moment from 'moment';

moment.updateLocale('en', {
  relativeTime: {
    s: '1s',
    ss: '%ds',
    m: '1min',
    mm: '%dmin',
    h: '1h',
    hh: '%dh',
    d: '1d',
    dd: '%dd',
  },
});


const STATUSES_ORDER = [
  'error',
  'pending-action',
  'waiting-approval',
  'needs-reconnect',
  'no-data',
  'done',
];

export function getTopStatus(statuses: string[]) {
  // eslint-disable-next-line no-restricted-syntax
  for (const status of STATUSES_ORDER) {
    if (statuses.includes(status)) {
      return status;
    }
  }
  return statuses[0];
}

export function getIntegrationStatus(integration: {
  status: string,
  updatedAt: Date | string
}) {
  const errorThresholdReached = isCurrentDateAfterGivenWorkingDays(
    integration.updatedAt, ERROR_BANNER_WORKING_DAYS_DISPLAY);
  if (integration.status === 'done'
    || (integration.status === 'error' && !errorThresholdReached)) {
    return 'done';
  }

  if (integration.status === 'error' && errorThresholdReached) {
    return 'error';
  }

  return integration.status;
}



export function formatLastSynced(integration: {
  lastProcessedAt: Date | string | undefined
}) {
  if (!integration.lastProcessedAt) {
    return undefined;
  }
  const value = moment(integration.lastProcessedAt);
  return ({
    value,
    absolute: moment(integration.lastProcessedAt).format('MMM DD, YYYY HH:mm'),
    relative: `last data detected and synced ${moment(integration.lastProcessedAt).fromNow()}`,
  });
}
