export interface FilterQuery {
  filter: any,
  body: any,
  orderBy: string,
  page?: number,
  perPage?: number,
}
