import Papa from 'papaparse';

export class CsvParser {

  static parse(data, config) {
    return Papa.parse(data, {
      header: true,
      skipEmptyLines: true,
      ...config
    });
  }
}