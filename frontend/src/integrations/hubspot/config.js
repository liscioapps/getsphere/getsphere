import config from '@/config';
import HubspotConnect from './components/hubspot-connect.vue';
import HubspotSettings
  from '@/integrations/hubspot/components/hubspot-settings.vue';

export default {
  enabled: config.isHubspotIntegrationEnabled,
  hideAsIntegration: config.isHubspotHided,
  name: 'HubSpot',
  backgroundColor: '#FFFFFF',
  borderColor: '#FFFFFF',
  description: 'Create a 2-way sync with HubSpot.',
  image:
    '/images/integrations/hubspot.svg',
  connectComponent: HubspotConnect,
  url: (username) => null,
  enterprise: true,
  chartColor: '#FF712E',
  showProfileLink: true,
  activityDisplay: {
    showLinkToUrl: true,
  },
  conversationDisplay: {
    replyContent: (conversation) => ({
      icon: 'ri-reply-line',
      copy: 'reply',
      number: conversation.activityCount - 1,
    }),
  },
  organization: {
    handle: (identity) => identity.name,
  },
};
