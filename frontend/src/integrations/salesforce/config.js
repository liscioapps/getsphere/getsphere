import config from '@/config';
import AppSalesforceConnect from './components/salesforce-connect.vue';

export default {
  enabled: config.isSalesforceIntegrationEnabled,
  hideAsIntegration: config.isSalesforceHided,
  name: 'Salesforce',
  connectComponent: AppSalesforceConnect,
  description: 'Create a 2-way sync with your Salesforce CRM.',
  image:
    '/images/integrations/salesforce.png',
  enterprise: true,
};
