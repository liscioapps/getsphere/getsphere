import AuthCurrentTenant from '@/modules/auth/auth-current-tenant';
import authAxios from '@/shared/axios/auth-axios';

export class SalesforceApiService {
  static syncMember(memberId: string): Promise<any> {
    const tenantId = AuthCurrentTenant.get();

    return authAxios.post(
      `/tenant/${tenantId}/salesforce-sync-member`,
      {
        memberId,
      },
    )
      .then((response) => response.data);
  }

  static stopSyncMember(memberId: string): Promise<any> {
    const tenantId = AuthCurrentTenant.get();

    return authAxios.post(
      `/tenant/${tenantId}/salesforce-stop-sync-member`,
      {
        memberId,
      },
    )
      .then((response) => response.data);
  }

  static syncOrganization(organizationId: string): Promise<any> {
    const tenantId = AuthCurrentTenant.get();

    return authAxios.post(
      `/tenant/${tenantId}/salesforce-sync-organization`,
      {
        organizationId,
      },
    )
      .then((response) => response.data);
  }

  static stopSyncOrganization(organizationId: string): Promise<any> {
    const tenantId = AuthCurrentTenant.get();

    return authAxios.post(
      `/tenant/${tenantId}/salesforce-stop-sync-organization`,
      {
        organizationId,
      },
    )
      .then((response) => response.data);
  }
}
