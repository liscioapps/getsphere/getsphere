import config from '@/config';
import RedditSettings
  from '@/integrations/reddit/components/reddit-settings.vue';
import RedditTitle
  from '@/integrations/reddit/components/reddit-title.vue';
import RedditConnect from './components/reddit-connect.vue';

export default {
  enabled: config.isRedditIntegrationEnabled,
  hideAsIntegration: config.isRedditHided,
  name: 'Reddit',
  backgroundColor: '#ffd8ca',
  borderColor: '#ffd8ca',
  description:
    'Connect Reddit to sync posts and comments from selected subreddits.',
  onboard: {
    description: 'Sync posts and comments from selected subreddits.',
  },
  image: '/images/integrations/reddit.svg',
  connectComponent: RedditConnect,
  settingsComponent: RedditSettings,
  titleComponent: RedditTitle,
  multiple: true,
  url: ({ username }) => (username
    ? `https://reddit.com/user/${username}`
    : null),
  chartColor: '#FF4500',
  showProfileLink: true,
  activityDisplay: {
    showLinkToUrl: true,
  },
  conversationDisplay: {
    replyContent: (conversation) => ({
      icon: 'ri-reply-line',
      copy: 'reply',
      number: conversation.activityCount - 1,
    }),
  },
  organization: {
    handle: (identity) => (identity.url
      ? identity.url.split('/')
        .at(-1)
      : identity.name),
  },
};
