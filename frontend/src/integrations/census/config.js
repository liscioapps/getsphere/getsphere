import config from '@/config';

export default {
  enabled: config.isCensusIntegrationEnabled,
  hideAsIntegration: config.isCensusHided,
  name: 'Census',
  description: 'Use Census Reverse ETL to send customer data to GetSphere.dev.',
  image:
      '/images/integrations/census.png',
  enterprise: true,
};
