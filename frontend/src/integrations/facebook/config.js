import config from '@/config';

export default {
  image: '/images/integrations/facebook.png',
  hideAsIntegration: config.isFacebookHided,
  enabled: config.isFacebookEnabled,
  name: 'Facebook',
  description:
  'Connect Facebook to sync profile information, photos and videos, messages.',
  organization: {
    handle: (identity) => (identity.url ? identity.url.split('/').at(-1) : identity.name),
  },
};
