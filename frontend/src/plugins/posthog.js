import posthog from 'posthog-js';
import config from '@/config';

export default {
  install(app) {
    if (typeof window !== 'undefined' && config.posthogKey) {
      app.config.globalProperties.$posthog = posthog.init(
        config.posthogKey,
        {
          api_host: 'https://us.i.posthog.com',
          capture_pageview: false,
        },
      );
      (window).analytics = {
        track: (name, data) => {
          posthog.capture(name, data);
        },
        identify: (id, data) => {
          posthog.identify(id, data);
        },
        page: (name, data) => {
          posthog.capture('$pageview', { page: name, ...data });
        },
      };
    }
  },
};

export const identifyTenant = (currentTenant) => {
  if (config.posthogKey) {
    posthog.group('tenant', currentTenant.id, {
      name: `${currentTenant.name} (${currentTenant.id})`,
      plan: currentTenant.plan,
      isTrialPlan: currentTenant.isTrialPlan,
    });
  }
};

export const identifyUser = (currentUser) => {
  if (config.posthogKey) {
    console.log('Identifying user for Posthog.');
    posthog.identify(currentUser.id, {
      email: currentUser.email,
      name: currentUser.firstName,
    });
  }
};
