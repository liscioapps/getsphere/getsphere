export type GetEnrichmentSocial = {
  social: string
  url: string
}

export type GetEnrichmentParams = {
  name?: string
  website?: string
  socials: GetEnrichmentSocial[]
}
