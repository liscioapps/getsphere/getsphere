import getCompanyByDomain from '../api/getCompanyByDomain'
import getCompanyByName from '../api/getCompanyByName'
import getCompanyBySocial from '../api/getCompanyBySocial'
import { CompanyData } from '../api/types'
import { GetEnrichmentParams } from '../types'

export const getEnrichment = async (params: GetEnrichmentParams): Promise<CompanyData | null> => {
  if (params.website) {
    const resp = await getCompanyByDomain(params.website)
    if (resp) {
      return resp
    }
  }
  if (params.name) {
    const resp = await getCompanyByName(params.name)
    if (resp.companies.length) {
      return resp.companies[0]
    }
  }
  for (const social of params.socials) {
    const resp = await getCompanyBySocial(social.social, social.url)
    if (resp) {
      return resp
    }
  }
  return null
}
