import { renameKeys } from '@crowd/common'
import { CompanyData } from '../api/types'

function parseRange(value: string): {
  min?: number
  max?: number
} {
  if (!value) {
    return null
  }
  if (value.startsWith('under')) {
    const max = parseFloat(value.split('-')[1].replace('m', '')) * 1_000_000
    return { max }
  } else if (value.startsWith('over')) {
    const min = parseFloat(value.split('-')[1].replace('b', '')) * 1_000_000_000
    return { min }
  } else {
    const [minStr, maxStr] = value.split('-')
    const min = parseFloat(minStr.replace('m', '')) * 1_000_000
    const maxUnit = maxStr.includes('b') ? 1_000_000_000 : 1_000_000
    const max = parseFloat(maxStr.replace('m', '').replace('b', '')) * maxUnit
    return { min, max }
  }
}

function formatAddress(company: CompanyData): string {
  const city = company.city?.name || ''
  const county = company.county?.name || ''
  const state = company.state?.name || ''
  const country = company.country?.name || ''
  const postcode = company.city?.postcode || ''
  const elements = [city, county, state, country, postcode].filter((e) => e)
  return elements.join(', ')
}

export async function mapEnrichmentData(apiData: CompanyData) {
  const data = renameKeys(apiData, {
    totalEmployeesExact: 'employees',
    industryMain: 'industry',
    domainAlts: 'alternativeDomains',
    domain: 'url',
    descriptionShort: 'headline',
    totalEmployees: 'size',
    yearnFounded: 'founded',
    name: 'displayName',
    // location: 'address',
  })

  data.ultimateParent = apiData.companyParent?.name
  if (apiData.companiesSubsidiaries?.length) {
    data.allSubsidiaries = apiData.companiesSubsidiaries.map((s) => s.name)
  }

  if (apiData.phoneNumber) {
    data.phoneNumbers = [apiData.phoneNumber]
  }
  if (apiData.revenue) {
    data.revenueRange = parseRange(apiData.revenue)
  }
  if (apiData.socialNetworks) {
    if (apiData.socialNetworks.facebook || apiData.socialNetworks.facebookId) {
      data.facebook = {
        url: apiData.socialNetworks.facebook,
        handle: apiData.socialNetworks.facebookId,
      }
    }
    if (apiData.socialNetworks.linkedin || apiData.socialNetworks.linkedinIdAlpha) {
      data.linkedin = {
        url: apiData.socialNetworks.linkedin,
        handle: apiData.socialNetworks.linkedinIdAlpha,
      }
    }
    data.address = {
      country: apiData.country?.name,
      region: apiData.state?.name,
      continent: apiData.continent?.name,
      street_address: '',
      postal_code: apiData.city?.postcode,
      address_line_2: '',
      locality: apiData.city?.name,
      metro: '',
      name: '',
    }
    if (apiData.city) {
      data.geoLocation = `${apiData.city.latitude},${apiData.city.longitude}`
    }
  }
  return data
}
