import * as db from '@crowd/data-access-layer'
import { OrganizationCacheData, OrganizationData } from '@crowd/data-access-layer'
import { svc } from '../main'

export const getOrganizationCaches = async (page: number, pageSize: number) => {
  return await db.getEnrichableOrganizationCaches(svc.postgres.reader.connection(), {
    limit: pageSize,
    offset: page * pageSize,
  })
}

export const getOrganizationCache = async (id: string) => {
  return db.getOrganizationCache(svc.postgres.reader.connection(), id)
}

export const updateOrganizationCache = async (cache: OrganizationCacheData, updateData: any) => {
  return svc.postgres.writer.transactionally(async (tx) => {
    return db.updateOrganizationCache(tx.connection(), cache, updateData)
  })
}

export const markOrganizationCacheAsEnriched = async (id: string) => {
  return db.markOrganizationCacheAsEnriched(svc.postgres.writer.connection(), id)
}

export const getOrganizationsToSync = async (page: number, pageSize: number) => {
  return await db.getOrganizationsToSync(svc.postgres.reader.connection(), {
    limit: pageSize,
    offset: page * pageSize,
  })
}

export const getOrganization = async (id: string) => {
  return await db.getOrganization(svc.postgres.reader.connection(), id)
}

export const updateOrganization = async (organization: OrganizationData, updateData: any) => {
  return svc.postgres.writer.transactionally(async (tx) => {
    return db.updateOrganization(tx.connection(), organization, updateData)
  })
}
