import {
  PriorityLevelContextRepository,
  QueuePriorityContextLoader,
  SearchSyncWorkerEmitter,
} from '@crowd/common_services'
import { svc } from '../main'
import { getServiceChildLogger } from '@crowd/logging'

export async function triggerOpenSearchSync(
  tenantId: string,
  organizationId: string,
): Promise<void> {
  const log = getServiceChildLogger(triggerOpenSearchSync.name, {
    tenantId,
    organizationId,
  })
  try {
    const repo = new PriorityLevelContextRepository(svc.postgres?.reader, svc.log)
    const loader: QueuePriorityContextLoader = (tenantId: string) =>
      repo.loadPriorityLevelContext(tenantId)

    const searchSyncWorkerEmitter = new SearchSyncWorkerEmitter(
      svc.sqs,
      svc.redis,
      svc.tracer,
      svc.unleash,
      loader,
      svc.log,
    )

    await await searchSyncWorkerEmitter.init()

    await searchSyncWorkerEmitter.triggerOrganizationSync(tenantId, organizationId, false)
  } catch (err) {
    log.error('Failed triggering org sync', err)
    throw new Error(err)
  }
}
