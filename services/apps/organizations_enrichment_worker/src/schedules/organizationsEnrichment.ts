import { ScheduleAlreadyRunning, ScheduleOverlapPolicy } from '@temporalio/client'
import { svc } from '../main'
import { spawnOrganizationEnrichments } from '../workflows/spawnOrganizationEnrichments'

export async function scheduleOrganizationsEnrichment() {
  try {
    await svc.temporal?.schedule.create({
      scheduleId: 'organizations-enrichment',
      spec: {
        intervals: [
          {
            every: '1 hour',
          },
        ],
      },
      policies: {
        overlap: ScheduleOverlapPolicy.BUFFER_ONE,
        catchupWindow: '1 minute',
      },
      action: {
        type: 'startWorkflow',
        workflowType: spawnOrganizationEnrichments,
        taskQueue: 'organizations-enrichment',
        workflowExecutionTimeout: '1 hour',
      },
    })
  } catch (err) {
    if (err instanceof ScheduleAlreadyRunning) {
      svc.log.info('Schedule already registered in Temporal.')
      svc.log.info('Configuration may have changed since. Please make sure they are in sync.')
    } else {
      throw new Error(err)
    }
  }
}
