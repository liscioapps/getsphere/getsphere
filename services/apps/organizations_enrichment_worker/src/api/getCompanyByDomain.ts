import axios from 'axios'
import { CompanyData } from './types'

async function getCompanyByDomain(domain: string) {
  return await axios
    .get<CompanyData | null>(
      `https://api.thecompaniesapi.com/v1/companies/${domain}?token=${process.env['CROWD_COMPANIES_API_TOKEN']}`,
    )
    .then((r) => r.data)
}
export default getCompanyByDomain
