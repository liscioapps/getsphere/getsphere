import axios from 'axios'
import { CompanyData } from './types'

async function getCompanyBySocial(social: string, url: string) {
  return await axios
    .get<CompanyData | null>(
      `https://api.thecompaniesapi.com/v1/companies/by-social?${social}=${url}&token=${process.env['CROWD_COMPANIES_API_TOKEN']}`,
    )
    .then((r) => r.data)
}
export default getCompanyBySocial
