export type CompaniesResponse = {
  companies: CompanyData[]
  meta: PaginationData
}

export type CompanyData = {
  alexaRank: number
  businessType: null
  city: City
  codeNaics: string | null
  codeSic: string | null
  companiesAcquisitions: CompanyLink[]
  companiesSimilar: CompanyLink[]
  companiesSubsidiaries: CompanyLink[]
  companyParent: CompanyLink | null
  continent: Continent
  country: Continent
  county: County
  description: string
  descriptionShort: string
  domain: string
  domainAlts: string[]
  domainName: string
  domainTld: string
  emailPatterns: EmailPattern[]
  id: number
  industries: string[]
  industryMain: string
  logo: string
  monthlyVisitors: string
  name: string
  phoneNumber: string | null
  revenue: string
  socialNetworks: CompanySocialNetworks
  state: County
  stockExchange: null
  stockSymbol: null
  technologies: string[]
  technologyCategories: string[]
  totalEmployees: string
  totalEmployeesExact: number
  yearFounded: number
}

export type City = {
  address: null
  code: string
  latitude: string
  longitude: string
  name: string
  postcode: string
}

export type CompanyLink = {
  descriptionShort: string
  domain: string
  id: number
  name: string
  similitude?: string
  socialNetworks?: CompanySocialNetworks
}

export type CompanySocialNetworks = {
  facebook: string
  facebookId: string
  instagram: string
  instagramId: string
  linkedin: string
  linkedinIdAlpha: string
  linkedinIdNumeric: number
  linkedinSalesNavigator: string
  twitter: string
  twitterId: string
  youtube: string
  youtubeId: string
  pinterest?: string
  pinterestId?: string
}

export type Continent = {
  code: string
  latitude: string
  longitude: string
  name: string
  nameEs: string
  nameFr: string
}

export type County = {
  code: string
  latitude: string
  longitude: string
  name: string
}

export type EmailPattern = {
  pattern: string
  usagePercentage: number
}

export type PaginationData = {
  currentPage: number
  firstPage: number
  lastPage: number
  perPage: number
  total: number
}
