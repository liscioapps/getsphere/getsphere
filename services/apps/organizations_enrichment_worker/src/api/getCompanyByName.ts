import axios from 'axios'
import { CompaniesResponse } from './types'

function normalizeName(name: string) {
  if (!name) {
    return ''
  }
  const tokens = [
    'Inc.',
    'Corp.',
    'LLC',
    'Ltd.',
    'Co.',
    'GmbH',
    'AG',
    'S.A.',
    'S.L.',
    'S.L.U.',
    'S.R.L.',
    'Pte. Ltd.',
    'Pvt. Ltd.',
    'B.V.',
    'N.V.',
  ]

  for (const token of tokens) {
    name = name.replace(token, '').trim()
  }
  return name
}

async function getCompanyByName(name: string) {
  return await axios
    .get<CompaniesResponse>(
      `https://api.thecompaniesapi.com/v1/companies/by-name?name=${normalizeName(name)}&token=${
        process.env['CROWD_COMPANIES_API_TOKEN']
      }`,
    )
    .then((r) => r.data)
}
export default getCompanyByName
