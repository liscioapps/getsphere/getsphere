import { type OrganizationCacheListData } from '@crowd/data-access-layer'
import { PlatformType } from '@crowd/types'
import { proxyActivities } from '@temporalio/workflow'
import * as activities from '../activities'
import { GetEnrichmentSocial } from '../types'

const aCtx = proxyActivities<typeof activities>({
  startToCloseTimeout: '1 minute',
})

const SOCIAL_PLATFORMS = [PlatformType.GITHUB, PlatformType.LINKEDIN, PlatformType.TWITTER]

export async function enrichOrganization(orgCache: OrganizationCacheListData) {
  const cacheData = await aCtx.getOrganizationCache(orgCache.id)

  if (!cacheData) {
    return
  }

  const name = cacheData.identities.find((i) => !!i.name)?.name
  const website = cacheData.identities.find((i) => !!i.website)?.website

  const socials: GetEnrichmentSocial[] = []

  for (const platform of SOCIAL_PLATFORMS) {
    const social = cacheData[platform]
    if (social?.url) {
      socials.push({
        social: social.platform,
        url: social.url,
      })
    }
  }

  if (!name && !website && socials.length === 0) {
    await aCtx.markOrganizationCacheAsEnriched(orgCache.id)
    return
  }

  const enrichment = await aCtx.getEnrichment({
    name,
    website,
    socials,
  })

  if (enrichment) {
    const updateData = await aCtx.mapEnrichmentData(enrichment)
    await aCtx.updateOrganizationCache(cacheData, updateData)
  } else {
    await aCtx.markOrganizationCacheAsEnriched(orgCache.id)
  }
  // fetch organization data
}
