import { OrganizationCacheListData } from '@crowd/data-access-layer'
import {
  ChildWorkflowCancellationType,
  ParentClosePolicy,
  proxyActivities,
  startChild,
} from '@temporalio/workflow'
import * as activities from '../activities'
import { enrichOrganization } from './enrichOrganization'

const aCtx = proxyActivities<typeof activities>({
  startToCloseTimeout: '1 minute',
})

const PAGE_SIZE = 100

export async function spawnOrganizationEnrichments() {
  let page = 0
  let caches: OrganizationCacheListData[] = []

  do {
    caches = await aCtx.getOrganizationCaches(page, PAGE_SIZE)

    for (const cache of caches) {
      await startChild(enrichOrganization, {
        workflowId: 'organization-enrichment/' + cache.id,
        cancellationType: ChildWorkflowCancellationType.ABANDON,
        parentClosePolicy: ParentClosePolicy.PARENT_CLOSE_POLICY_ABANDON,
        workflowExecutionTimeout: '5 minutes',
        retry: {
          maximumAttempts: 3,
        },
        args: [cache],
      })
    }
    page++
  } while (caches.length > 0)
}
