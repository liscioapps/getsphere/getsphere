import { proxyActivities } from '@temporalio/workflow'
import * as activities from '../activities'
import { PlatformType } from '@crowd/types'

const aCtx = proxyActivities<typeof activities>({
  startToCloseTimeout: '1 minute',
})

const SYNC_PLATFORMS = [
  {
    platform: PlatformType.GITHUB,
    baseUrl: 'https://github.com/',
  },
  {
    platform: PlatformType.LINKEDIN,
    baseUrl: 'https://linkedin.com/in/',
  },
  {
    platform: PlatformType.TWITTER,
    baseUrl: 'https://twitter.com/',
  },
]

export async function syncOrganizationsWithCache(args: { cacheId: string; id: string }) {
  const cacheData = await aCtx.getOrganizationCache(args.cacheId)
  const organizationData = await aCtx.getOrganization(args.id)

  if (!cacheData || !organizationData) {
    return
  }

  const updateData = {
    ...cacheData,
    identities: organizationData.identities,
  }

  SYNC_PLATFORMS.forEach(({ baseUrl, platform }) => {
    if (
      cacheData[platform]?.handle &&
      !updateData.identities.some((i) => i.platform === platform)
    ) {
      updateData.identities.push({
        name: cacheData[platform].handle,
        platform: platform,
        url: cacheData[platform].url || `${baseUrl}${cacheData[platform].handle}`,
      })
    }
  })
  await aCtx.updateOrganization(organizationData, updateData)
  await aCtx.triggerOpenSearchSync(organizationData.tenantId, organizationData.id)
}
