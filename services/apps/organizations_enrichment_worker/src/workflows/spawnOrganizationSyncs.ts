import {
  ChildWorkflowCancellationType,
  ParentClosePolicy,
  proxyActivities,
  startChild,
} from '@temporalio/workflow'
import * as activities from '../activities'
import { syncOrganizationsWithCache } from './syncOrganizationWithCache'

const aCtx = proxyActivities<typeof activities>({
  startToCloseTimeout: '1 minute',
})

const PAGE_SIZE = 100

export async function spawnOrganizationSyncs() {
  let page = 0
  let orgs: { id: string; cacheId: string }[] = []

  do {
    orgs = await aCtx.getOrganizationsToSync(page, PAGE_SIZE)

    for (const org of orgs) {
      await startChild(syncOrganizationsWithCache, {
        workflowId: 'organization-enrichment-sync/' + org.cacheId + '/' + org.id,
        cancellationType: ChildWorkflowCancellationType.ABANDON,
        parentClosePolicy: ParentClosePolicy.PARENT_CLOSE_POLICY_ABANDON,
        workflowExecutionTimeout: '5 minutes',
        retry: {
          maximumAttempts: 3,
        },
        args: [org],
      })
    }
    page++
  } while (orgs.length > 0)
}
