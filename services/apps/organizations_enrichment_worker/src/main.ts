import { Config } from '@crowd/archetype-standard'
import { ServiceWorker, Options } from '@crowd/archetype-worker'
import { scheduleOrganizationsEnrichment } from './schedules/organizationsEnrichment'
import { scheduleOrganizationsCacheSync } from './schedules/organizationsCacheSync'

const config: Config = {
  envvars: [],
  producer: {
    enabled: false,
  },
  temporal: {
    enabled: true,
  },
  redis: {
    enabled: true,
  },
}

const options: Options = {
  postgres: {
    enabled: true,
  },
  sqs: {
    enabled: true,
  },
}

export const svc = new ServiceWorker(config, options)

setImmediate(async () => {
  if (!process.env['CROWD_COMPANIES_API_TOKEN']) {
    throw new Error('CROWD_COMPANIES_API_TOKEN envvar is required')
  }

  await svc.init()

  await scheduleOrganizationsEnrichment()
  await scheduleOrganizationsCacheSync()

  await svc.start()
})
