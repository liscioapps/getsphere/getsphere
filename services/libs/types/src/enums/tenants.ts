import { FeatureFlag } from './featureFlags'

export enum TenantPlans {
  Basic = 'Basic',
  Starter = 'Starter',
  Growth = 'Growth',
  EagleEye = 'Eagle Eye',
  Business = 'Business',
  Enterprise = 'Enterprise',
}

export const PLAN_LIMITS = {
  [TenantPlans.Basic]: {
    [FeatureFlag.USERS]: 1,
    [FeatureFlag.ACTIVE_CONTACTS]: 500,
  },
  [TenantPlans.Starter]: {
    [FeatureFlag.AUTOMATIONS]: 2,
    [FeatureFlag.CSV_EXPORT]: 2,
    [FeatureFlag.USERS]: 3,
    [FeatureFlag.ACTIVE_CONTACTS]: 2000,
  },
  [TenantPlans.Growth]: {
    [FeatureFlag.AUTOMATIONS]: 10,
    [FeatureFlag.CSV_EXPORT]: 10,
    [FeatureFlag.MEMBER_ENRICHMENT]: 1000,
    [FeatureFlag.ORGANIZATION_ENRICHMENT]: 200,
  },
  [TenantPlans.Business]: {
    [FeatureFlag.AUTOMATIONS]: 20,
    [FeatureFlag.CSV_EXPORT]: 20,
    [FeatureFlag.MEMBER_ENRICHMENT]: Infinity,
    [FeatureFlag.ORGANIZATION_ENRICHMENT]: Infinity,
    [FeatureFlag.USERS]: Infinity,
    [FeatureFlag.ACTIVE_CONTACTS]: 10000,
  },
  [TenantPlans.Enterprise]: {
    [FeatureFlag.USERS]: Infinity,
    [FeatureFlag.AUTOMATIONS]: Infinity,
    [FeatureFlag.CSV_EXPORT]: Infinity,
    [FeatureFlag.MEMBER_ENRICHMENT]: Infinity,
    [FeatureFlag.ORGANIZATION_ENRICHMENT]: Infinity,
    [FeatureFlag.ACTIVE_CONTACTS]: Infinity,
  },
}
