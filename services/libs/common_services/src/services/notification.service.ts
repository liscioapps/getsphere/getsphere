import axios from 'axios'

export class NotificationService {
  async notify(message: string) {
    if (!process.env['CROWD_NOTIFICATION_WEBHOOK']) {
      return
    }

    await axios.post(process.env['CROWD_NOTIFICATION_WEBHOOK'], {
      text: message,
    })
  }
}
