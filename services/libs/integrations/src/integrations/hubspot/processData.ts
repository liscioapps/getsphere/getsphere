import { IntegrationResultType } from '@crowd/types'
import { ProcessDataHandler } from '../../types'
import { mapMemberFromHubSpot, mapOrganizationFromHubSpot } from './mapping'
import {
  HubSpotContactPublishData,
  HubSpotStreamType,
  IHubSpotIntegrationSettings,
  IHubSpotPublishData,
} from './types'

const processContact: ProcessDataHandler = async (ctx) => {
  const data = ctx.data as IHubSpotPublishData
  const contact = data.entity as HubSpotContactPublishData
  const settings = ctx.integration.settings as IHubSpotIntegrationSettings

  const member = mapMemberFromHubSpot(settings.hubId, contact)
  await ctx.publishCustom(member, IntegrationResultType.MEMBER_ENRICH)
}

const processCompany: ProcessDataHandler = async (ctx) => {
  const data = ctx.data as IHubSpotPublishData
  const settings = ctx.integration.settings as IHubSpotIntegrationSettings
  const organization = mapOrganizationFromHubSpot(settings.hubId, data.entity)
  await ctx.publishCustom(organization, IntegrationResultType.ORGANIZATION_ENRICH)
}

const handler: ProcessDataHandler = async (ctx) => {
  const data = ctx.data as IHubSpotPublishData
  switch (data.type) {
    case HubSpotStreamType.CONTACTS:
      await processContact(ctx)
      break
    case HubSpotStreamType.COMPANIES:
      await processCompany(ctx)
      break
  }
}

export default handler
