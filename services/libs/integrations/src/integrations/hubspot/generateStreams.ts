import { GenerateStreamsHandler } from '../../types'
import { HubSpotEntity, HubSpotStreamType, IHubSpotIntegrationSettings } from './types'

const handler: GenerateStreamsHandler = async (ctx) => {
  const settings = ctx.integration.settings as IHubSpotIntegrationSettings
  if (settings.enabledFor.includes(HubSpotEntity.ORGANIZATIONS)) {
    await ctx.publishStream(HubSpotStreamType.COMPANIES)
  }

  if (settings.enabledFor.includes(HubSpotEntity.MEMBERS)) {
    await ctx.publishStream(HubSpotStreamType.CONTACTS)
  }
}

export default handler
