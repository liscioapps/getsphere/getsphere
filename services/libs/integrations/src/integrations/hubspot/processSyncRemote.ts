import {
  AutomationSyncTrigger,
  Entity,
  HubspotSettings,
  IMember,
  IMemberIdentity,
  IOrganization,
  IOrganizationIdentity,
  PlatformType,
} from '@crowd/types'
import {
  IBatchIntegrationOperationResult,
  IIntegrationProcessRemoteSyncContext,
  ProcessIntegrationSyncHandler,
} from '../../types'
import {
  hubspotAddContactToList,
  hubSpotCreateCompanies,
  hubSpotCreateContacts,
  hubSpotUpdateCompanies,
} from './api'
import { mapMemberToHubSpot, mapOrganizationToHubSpot } from './mapping'

const getHubSpotId = ({
  identities,
}: {
  identities: IMemberIdentity[] | IOrganizationIdentity[]
}) => {
  return identities.find((identity) => identity.platform === PlatformType.HUBSPOT)?.sourceId
}

type ProcessEntityHandler<T> = (
  toCreate: T[],
  toUpdate: T[],
  ctx: IIntegrationProcessRemoteSyncContext,
) => Promise<IBatchIntegrationOperationResult>

const processMembers: ProcessEntityHandler<IMember> = async (toCreate, toUpdate, ctx) => {
  const result: IBatchIntegrationOperationResult = { created: [], updated: [] }
  if (toCreate.length) {
    const contacts = toCreate.map(mapMemberToHubSpot)
    const createResult = await hubSpotCreateContacts(ctx, contacts)
    result.created = toCreate.map((c, index) => ({
      lastSyncedPayload: c,
      memberId: c.id,
      sourceId: createResult.results[index].id,
    }))
  }
  if (toUpdate.length) {
    const contacts = toUpdate.map(mapMemberToHubSpot).map((contact, i) => ({
      ...contact,
      id: getHubSpotId(toUpdate[i]),
    }))
    const updateResult = await hubSpotCreateContacts(ctx, contacts)
    result.updated = toCreate.map((c, index) => ({
      lastSyncedPayload: c,
      memberId: c.id,
      sourceId: updateResult.results[index].id,
    }))
  }
  return { created: [], updated: [] }
}

const processOrganizations: ProcessEntityHandler<IOrganization> = async (
  toCreate,
  toUpdate,
  ctx,
) => {
  const result: IBatchIntegrationOperationResult = { created: [], updated: [] }
  if (toCreate.length) {
    const companies = toCreate.map(mapOrganizationToHubSpot)
    const createResult = await hubSpotCreateCompanies(ctx, companies)
    result.created = toCreate.map((c, index) => ({
      lastSyncedPayload: c,
      organizationId: c.id,
      sourceId: createResult.results[index].id,
    }))
  }
  if (toUpdate.length) {
    const companies = toUpdate.map(mapOrganizationToHubSpot).map((company, i) => ({
      ...company,
      id: getHubSpotId(toUpdate[i]),
    }))
    const updateResult = await hubSpotUpdateCompanies(ctx, companies)
    result.updated = toCreate.map((c, index) => ({
      lastSyncedPayload: c,
      organizationId: c.id,
      sourceId: updateResult.results[index].id,
    }))
  }

  if (ctx.automation && ctx.automation.trigger === AutomationSyncTrigger.MEMBER_ATTRIBUTES_MATCH) {
    const settings = ctx.automation.settings as HubspotSettings
    if (settings.contactList && !isNaN(+settings.contactList)) {
      const ids = result.created.map((r) => r.sourceId)
      await hubspotAddContactToList(ctx, +settings.contactList, ids)
    }
  }
  return result
}

const handler: ProcessIntegrationSyncHandler = async (toCreate, toUpdate, entity, ctx) => {
  switch (entity) {
    case Entity.MEMBERS:
      return await processMembers(toCreate as IMember[], toUpdate as IMember[], ctx)
    case Entity.ORGANIZATIONS:
      return await processOrganizations(
        toCreate as IOrganization[],
        toUpdate as IOrganization[],
        ctx,
      )
    default:
      throw new Error(`Unsupported entity: ${entity}`)
  }
}

export default handler
