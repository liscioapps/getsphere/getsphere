import { PlatformType } from '@crowd/types'
import { IIntegrationDescriptor } from '../../types'
import generateStreams from './generateStreams'
import { HUBSPOT_MEMBER_ATTRIBUTES } from './memberAttributes'
import processData from './processData'
import processStream from './processStream'
import startSyncRemote from './startSyncRemote'
import processSyncRemote from './processSyncRemote'

const descriptor: IIntegrationDescriptor = {
  type: PlatformType.HUBSPOT,
  memberAttributes: HUBSPOT_MEMBER_ATTRIBUTES,
  checkEvery: 4 * 60,
  generateStreams,
  processStream,
  processData,
  startSyncRemote,
  processSyncRemote,
}

export default descriptor
