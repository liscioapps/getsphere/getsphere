import { ProcessStreamHandler } from '../../types'
import { getHubSpotCompanies, getHubspotContacts } from './api'
import { HubSpotStreamType, IHubSpotPublishData } from './types'

const processCompanies: ProcessStreamHandler = async (ctx) => {
  const companies = await getHubSpotCompanies(ctx)
  for (const company of companies) {
    await ctx.processData<IHubSpotPublishData>({
      type: HubSpotStreamType.COMPANIES,
      entity: company,
    })
  }
}

const processContacts: ProcessStreamHandler = async (ctx) => {
  const contacts = await getHubspotContacts(ctx)
  for (const contact of contacts) {
    await ctx.processData<IHubSpotPublishData>({
      type: HubSpotStreamType.CONTACTS,
      entity: contact,
    })
  }
}

const handler: ProcessStreamHandler = async (ctx) => {
  switch (ctx.stream.identifier) {
    case HubSpotStreamType.COMPANIES:
      await processCompanies(ctx)
      break
    case HubSpotStreamType.CONTACTS:
      await processContacts(ctx)
      break
    default:
      await ctx.abortRunWithError(`Unknown stream type: ${ctx.stream.identifier}`)
      break
  }
  //
}

export default handler
