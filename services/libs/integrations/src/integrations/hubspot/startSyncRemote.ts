import { AutomationState, AutomationSyncTrigger, IAutomationData } from '@crowd/types'
import { StartIntegrationSyncHandler } from '../../types'
import { HubSpotEntity, IHubSpotIntegrationSettings } from './types'

const handler: StartIntegrationSyncHandler = async (ctx) => {
  const settings = ctx.integration.settings as IHubSpotIntegrationSettings
  const activeAutomations = ctx.automations.filter(
    (automation) => automation.state === AutomationState.ACTIVE,
  )

  const automations: IAutomationData[] = []

  if (settings.enabledFor.includes(HubSpotEntity.MEMBERS)) {
    await ctx.integrationSyncWorkerEmitter.triggerSyncMarkedMembers(
      ctx.tenantId,
      ctx.integration.id,
    )
    automations.push(
      ...activeAutomations.filter(
        (automation) => automation.trigger === AutomationSyncTrigger.MEMBER_ATTRIBUTES_MATCH,
      ),
    )
  }

  if (settings.enabledFor.includes(HubSpotEntity.ORGANIZATIONS)) {
    await ctx.integrationSyncWorkerEmitter.triggerSyncMarkedOrganizations(
      ctx.tenantId,
      ctx.integration.id,
    )
    automations.push(
      ...activeAutomations.filter(
        (automation) => automation.trigger === AutomationSyncTrigger.ORGANIZATION_ATTRIBUTES_MATCH,
      ),
    )
  }

  for (const automation of automations) {
    await ctx.integrationSyncWorkerEmitter.triggerOnboardAutomation(
      ctx.tenantId,
      ctx.integration.id,
      automation.id,
      automation.trigger as AutomationSyncTrigger,
    )
  }
}

export default handler
