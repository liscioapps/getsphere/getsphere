import { PlatformType } from '@crowd/types'
import { Client } from '@hubspot/api-client'
import {
  SimplePublicObjectBatchInput,
  SimplePublicObjectInputForCreate,
} from '@hubspot/api-client/lib/codegen/crm/contacts'
import {
  ListSearchResponse,
  PublicObjectListSearchResult,
} from '@hubspot/api-client/lib/codegen/crm/lists'
import { NangoContext } from '../../types'
import { getNangoToken } from '../nango'
import {
  HUBSPOT_MEMBER_ATTRIBUTES_MAPPING,
  HUBSPOT_MEMBER_DEFAULT_PROPERTIES,
  HUBSPOT_MEMBER_PLATFORMS_MAPPING,
  HUBSPOT_ORGANIZATION_ATTRIBUTES_MAPPING,
  HUBSPOT_ORGANIZATION_DEFAULT_PROPERTIES,
} from './mapping'
import { HubSpotContactPublishData } from './types'

export const getHubSpotClient = async (ctx: NangoContext) => {
  const accessToken = await getNangoToken(ctx.serviceSettings.nangoId, PlatformType.HUBSPOT, ctx)
  const hubspotClient = new Client({ accessToken })
  return hubspotClient
}

export const getHubspotLists = async (ctx: NangoContext) => {
  const client = await getHubSpotClient(ctx)
  const result: PublicObjectListSearchResult[] = []

  let res: ListSearchResponse
  do {
    res = await client.crm.lists.listsApi.doSearch({ offset: 0, additionalProperties: [] })
    result.push(...res.lists)
  } while (res.hasMore)

  return result
}

const getCompanyProperties = () => {
  return [
    ...HUBSPOT_ORGANIZATION_DEFAULT_PROPERTIES,
    ...Object.values(HUBSPOT_ORGANIZATION_ATTRIBUTES_MAPPING),
  ]
}

export const getHubspotContacts = async (ctx: NangoContext) => {
  const properties = [
    ...HUBSPOT_MEMBER_DEFAULT_PROPERTIES,
    ...Object.values(HUBSPOT_MEMBER_ATTRIBUTES_MAPPING),
    ...Object.values(HUBSPOT_MEMBER_PLATFORMS_MAPPING),
  ]
  const client = await getHubSpotClient(ctx)
  const contacts = (await client.crm.contacts.getAll(undefined, undefined, properties, undefined, [
    'companies',
  ])) as HubSpotContactPublishData[]

  for (const contact of contacts) {
    if (contact.associations?.companies?.results.length) {
      const ids = contact.associations.companies.results.map((result) => result.id)
      const companies = await client.crm.companies.batchApi.read({
        inputs: ids.map((id) => ({ id })),
        properties: getCompanyProperties(),
        propertiesWithHistory: [],
      })
      contact.companies = companies.results
    }
  }

  return contacts
}

export const getHubSpotCompanies = async (ctx: NangoContext) => {
  const properties = getCompanyProperties()
  const client = await getHubSpotClient(ctx)
  return client.crm.companies.getAll(undefined, undefined, properties)
}

export const getHubSpotInfo = async (ctx: NangoContext) => {
  const accessToken = await getNangoToken(ctx.serviceSettings.nangoId, PlatformType.HUBSPOT, ctx)
  const hubspotClient = new Client({ accessToken })
  return hubspotClient.oauth.accessTokensApi.get(accessToken)
}

const createContacts = async (
  client: Client,
  members: SimplePublicObjectInputForCreate[],
  conflicts: SimplePublicObjectBatchInput[],
) => {
  try {
    const res = await client.crm.contacts.batchApi.create({
      inputs: members,
    })
    return { result: res, conflicts }
  } catch (e) {
    if (e.code === 409) {
      // error message: Contact already exists. Existing ID: {id}
      const regex = /Existing ID: (\d+)/
      const id = regex.exec(e.body.message)?.[1]
      if (!id) {
        throw new Error('Could not parse existing ID')
      }
      const contact = await client.crm.contacts.basicApi.getById(id)
      const conflict = members.find((c) => c.properties.email === contact.properties.email)
      return await createContacts(
        client,
        members.filter((m) => m.properties.email !== contact.properties.email),
        [
          ...conflicts,
          {
            ...conflict,
            id: id,
          },
        ],
      )
    } else {
      throw e
    }
  }
}

const updateContacts = async (client: Client, contacts: SimplePublicObjectBatchInput[]) => {
  return client.crm.contacts.batchApi.update({
    inputs: contacts,
  })
}

export const hubSpotCreateContacts = async (
  ctx: NangoContext,
  members: SimplePublicObjectInputForCreate[],
) => {
  const client = await getHubSpotClient(ctx)
  const { result, conflicts } = await createContacts(client, members, [])
  console.log({ result, conflicts })
  if (!conflicts.length) {
    return result
  }
  return updateContacts(client, conflicts)
}

export const hubSpotUpdateContacts = async (
  ctx: NangoContext,
  contacts: SimplePublicObjectBatchInput[],
) => {
  const client = await getHubSpotClient(ctx)
  return updateContacts(client, contacts)
}

const createCompanies = async (
  client: Client,
  companies: SimplePublicObjectInputForCreate[],
  conflicts: SimplePublicObjectBatchInput[],
) => {
  try {
    const res = await client.crm.companies.batchApi.create({
      inputs: companies,
    })
    return { result: res, conflicts }
  } catch (e) {
    if (e.code === 409) {
      // error message: Contact already exists. Existing ID: {id}
      const regex = /Existing ID: (\d+)/
      const id = regex.exec(e.body.message)?.[1]
      if (!id) {
        throw new Error('Could not parse existing ID')
      }
      const company = await client.crm.companies.basicApi.getById(id)
      const conflict = companies.find((c) => c.properties.domain === company.properties.domain)
      return await createCompanies(
        client,
        companies.filter((m) => m.properties.domain !== company.properties.domain),
        [
          ...conflicts,
          {
            ...conflict,
            id: id,
          },
        ],
      )
    } else {
      throw e
    }
  }
}

const updateCompanies = async (client: Client, companies: SimplePublicObjectBatchInput[]) => {
  return client.crm.companies.batchApi.update({
    inputs: companies,
  })
}

export const hubSpotCreateCompanies = async (
  ctx: NangoContext,
  companies: SimplePublicObjectInputForCreate[],
) => {
  const client = await getHubSpotClient(ctx)
  const { result, conflicts } = await createCompanies(client, companies, [])
  console.log({ result, conflicts })
  if (!conflicts.length) {
    return result
  }
  return updateCompanies(client, conflicts)
}

export const hubSpotUpdateCompanies = async (
  ctx: NangoContext,
  companies: SimplePublicObjectBatchInput[],
) => {
  const client = await getHubSpotClient(ctx)
  return updateCompanies(client, companies)
}

export const hubspotAddContactToList = async (ctx: NangoContext, listId: number, ids: number[]) => {
  const client = await getHubSpotClient(ctx)
  return client.crm.lists.membershipsApi.add(listId, ids)
}
