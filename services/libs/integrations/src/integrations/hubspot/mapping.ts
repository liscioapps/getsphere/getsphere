import {
  IMember,
  IMemberData,
  IOrganization,
  MemberAttributeName,
  OrganizationAttributeName,
  OrganizationSource,
  PlatformType,
} from '@crowd/types'
import { HubSpotContactPublishData, HubSpotOrganizationPublishData } from './types'
import {
  SimplePublicObjectBatchInput,
  SimplePublicObjectInputForCreate,
} from '@hubspot/api-client/lib/codegen/crm/contacts'
import { AssociationSpecAssociationCategoryEnum } from '@hubspot/api-client/lib/codegen/crm/contacts'

export const HUBSPOT_MEMBER_DEFAULT_PROPERTIES = ['email', 'firstname', 'lastname']
export const HUBSPOT_MEMBER_PLATFORMS_MAPPING = {
  [PlatformType.LINKEDIN]: 'hs_linkedinid',
  [PlatformType.TWITTER]: 'twitterhandle',
}
export const HUBSPOT_MEMBER_ATTRIBUTES_MAPPING = {
  [MemberAttributeName.JOB_TITLE]: 'jobtitle',
  [MemberAttributeName.WEBSITE_URL]: 'website',
  [MemberAttributeName.TIMEZONE]: 'hs_timezone',
}

export const HUBSPOT_ORGANIZATION_DEFAULT_PROPERTIES = [
  'name',
  'description',
  'address',
  'zip',
  'city',
  'country',
  'state',
  'linkedin_company_page',
  'hs_linkedin_handle',
  'twitterhandle',
  'industry',
]
export const HUBSPOT_ORGANIZATION_ATTRIBUTES_MAPPING = {
  [OrganizationAttributeName.DOMAIN]: 'domain',
  [OrganizationAttributeName.URL]: 'website',
}

export const mapOrganizationFromHubSpot = (
  hubId: string,
  data: HubSpotOrganizationPublishData,
): IOrganization => {
  const url = `https://app.hubspot.com/contacts/${hubId}/company/${data.id}`

  const organization: IOrganization = {
    identities: [
      {
        name: data.properties.name,
        platform: PlatformType.HUBSPOT,
        sourceId: data.id,
        url,
      },
    ],
    address: {
      country: data.properties.country,
      region: data.properties.state,
      continent: '',
      street_address: data.properties.address,
      postal_code: data.properties.zip,
      address_line_2: '',
      locality: data.properties.city,
      metro: '',
      name: data.properties.name,
    },
    linkedin: {
      url: data.properties.linkedin_company_page,
      handle: data.properties.hs_linkedin_handle,
    },
    twitter: {
      handle: data.properties.twitterhandle,
    },
    industry: data.properties.industry,
    attributes: {
      [OrganizationAttributeName.SOURCE_ID]: {
        [PlatformType.HUBSPOT]: data.id,
      },
      [OrganizationAttributeName.URL]: {
        [PlatformType.HUBSPOT]: url,
      },
    },
    source: OrganizationSource.HUBSPOT,
  }

  Object.keys(HUBSPOT_ORGANIZATION_ATTRIBUTES_MAPPING).forEach((key) => {
    if (data.properties[key]) {
      organization.attributes[key] = {
        [PlatformType.HUBSPOT]: data.properties[key],
      }
    }
  })

  return organization
}

export const mapMemberFromHubSpot = (
  hubId: string,
  contact: HubSpotContactPublishData,
): IMemberData => {
  const member: IMemberData = {
    identities: [
      {
        platform: PlatformType.HUBSPOT,
        username: contact.properties.email,
        sourceId: contact.id,
      },
    ],
    displayName: `${contact.properties?.firstname ?? ''} ${contact.properties?.lastname ?? ''}`,
    emails: contact.properties.email ? [contact.properties.email] : [],
    weakIdentities: [],
    attributes: {
      [MemberAttributeName.SOURCE_ID]: {
        [PlatformType.HUBSPOT]: contact.id,
      },
      [MemberAttributeName.URL]: {
        [PlatformType.HUBSPOT]: `https://app.hubspot.com/contacts/${hubId}/contact/${contact.id}`,
      },
    },
  }
  Object.keys(HUBSPOT_MEMBER_ATTRIBUTES_MAPPING).forEach((key) => {
    if (contact.properties[key]) {
      member.attributes[key] = {
        [PlatformType.HUBSPOT]: contact.properties[key],
      }
    }
  })
  Object.keys(HUBSPOT_MEMBER_PLATFORMS_MAPPING).forEach((key) => {
    if (contact[key]) {
      member.weakIdentities.push({
        platform: key,
        username: contact[key],
      })
    }
  })
  if (contact.companies) {
    member.organizations = contact.companies.map((company) =>
      mapOrganizationFromHubSpot(hubId, company),
    )
  }
  return member
}

export const mapMemberToHubSpot = (member: IMember): SimplePublicObjectInputForCreate => {
  const contact: SimplePublicObjectInputForCreate = {
    properties: {
      email: member.emails[0],
      firstname: member.displayName.split(' ')[0],
      lastname: member.displayName.split(' ')[1],
    },
    associations: [],
  }

  Object.keys(HUBSPOT_MEMBER_ATTRIBUTES_MAPPING).forEach((key) => {
    if (member.attributes[key] && member.attributes[key][PlatformType.HUBSPOT]) {
      contact.properties[key] = member.attributes[key][PlatformType.HUBSPOT]
    }
  })

  Object.keys(HUBSPOT_MEMBER_PLATFORMS_MAPPING).forEach((key) => {
    const platform = member.identities.find((identity) => identity.platform === key)
    if (platform) {
      contact.properties[key] = platform.username
    }
  })

  return contact
}

export const mapOrganizationToHubSpot = (
  organization: IOrganization,
): SimplePublicObjectInputForCreate => {
  const company: SimplePublicObjectInputForCreate = {
    properties: {
      name: organization.address.name,
      country: organization.address.country,
      state: organization.address.region,
      address: organization.address.street_address,
      zip: organization.address.postal_code,
      city: organization.address.locality,
      industry: organization.industry,
      linkedin_company_page: organization.linkedin.url,
      hs_linkedin_handle: organization.linkedin.handle,
      twitterhandle: organization.twitter.handle,
    },
    associations: [],
  }

  Object.keys(HUBSPOT_ORGANIZATION_ATTRIBUTES_MAPPING).forEach((key) => {
    if (organization.attributes[key] && organization.attributes[key][PlatformType.HUBSPOT]) {
      company.properties[key] = organization.attributes[key][PlatformType.HUBSPOT]
    }
  })

  return company
}
