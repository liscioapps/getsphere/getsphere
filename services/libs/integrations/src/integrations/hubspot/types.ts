import { SimplePublicObjectWithAssociations } from '@hubspot/api-client/lib/codegen/crm/contacts'

export interface IHubSpotIntegrationSettings {
  enabledFor: HubSpotEntity[]
  hubId: string
}

export enum HubSpotStreamType {
  CONTACTS = 'contacts',
  COMPANIES = 'companies',
}

export enum HubSpotEntity {
  MEMBERS = 'members',
  ORGANIZATIONS = 'organizations',
}

export type HubSpotOrganizationPublishData = SimplePublicObjectWithAssociations
export type HubSpotContactPublishData = SimplePublicObjectWithAssociations & {
  companies: HubSpotOrganizationPublishData[]
}

export interface IHubSpotPublishData {
  type: HubSpotStreamType
  entity: HubSpotOrganizationPublishData | HubSpotContactPublishData
}
