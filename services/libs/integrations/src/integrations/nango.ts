import { RequestThrottler } from '@crowd/common'
import axios from 'axios'
import { IGenerateStreamsContext, IProcessStreamContext, NangoContext } from '../types'

export const getNangoToken = async (
  connectionId: string,
  providerConfigKey: string,
  ctx: NangoContext,
  throttler?: RequestThrottler,
): Promise<string> => {
  try {
    const url = `${ctx.serviceSettings.nangoUrl}/connection/${connectionId}`
    const secretKey = ctx.serviceSettings.nangoSecretKey
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${secretKey}`,
    }

    ctx.log.debug({ secretKey, connectionId, providerConfigKey }, 'Fetching Nango token!')

    const params = {
      provider_config_key: providerConfigKey,
    }

    let response

    if (throttler) {
      response = await throttler.throttle(() => axios.get(url, { params, headers }))
    } else {
      response = await axios.get(url, { params, headers })
    }

    return response.data.credentials.access_token
  } catch (err) {
    ctx.log.error({ err }, 'Error while getting token from Nango')
    throw err
  }
}

export const getNangoConnection = async (
  connectionId: string,
  providerConfigKey: string,
  ctx: IProcessStreamContext | IGenerateStreamsContext | NangoContext,
  throttler?: RequestThrottler,
): Promise<{ instance_url: string; access_token: string }> => {
  try {
    const url = `${ctx.serviceSettings.nangoUrl}/connection/${connectionId}`
    const secretKey = ctx.serviceSettings.nangoSecretKey
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${secretKey}`,
    }

    ctx.log.debug({ secretKey, connectionId, providerConfigKey }, 'Fetching Nango connection!')

    const params = {
      provider_config_key: providerConfigKey,
    }

    let response

    if (throttler) {
      response = await throttler.throttle(() => axios.get(url, { params, headers }))
    } else {
      response = await axios.get(url, { params, headers })
    }

    return {
      instance_url: response.data.credentials.raw.instance_url,
      access_token: `Bearer ${response.data.credentials.access_token}`,
    }
  } catch (err) {
    ctx.log.error({ err }, 'Error while getting connection from Nango')
    throw err
  }
}
