// generateStreams.ts content
import { GenerateStreamsHandler } from '../../types'
import { ISalesforceIntegrationSettings, SalesforceEntity, SalesforceStreamType } from './types'

const handler: GenerateStreamsHandler = async (ctx) => {
  const settings = ctx.integration.settings as ISalesforceIntegrationSettings

  if (settings.enabledFor.includes(SalesforceEntity.MEMBERS)) {
    await ctx.publishStream(SalesforceStreamType.CONTACTS)
  }

  if (settings.enabledFor.includes(SalesforceEntity.ORGANIZATIONS)) {
    await ctx.publishStream(SalesforceStreamType.ORGANIZATIONS)
  }

  if (settings.enabledFor.includes(SalesforceEntity.ROOT)) {
    await ctx.publishStream(SalesforceStreamType.ROOT)
  }
}

export default handler
