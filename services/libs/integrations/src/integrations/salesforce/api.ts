import { PlatformType } from '@crowd/types'
import axios from 'axios'
import { getNangoConnection } from '../nango'
import { NangoContext } from '../../types'
import {
  SalesforceContactPublishData,
  SalesforceOrganizationPublishData,
  SalesforceUserPublishData,
} from './types'
const apiVersion = 'v60.0'

export async function getFromQuery(ctx: NangoContext, query: string) {
  const connection = await getNangoConnection(
    ctx.serviceSettings.nangoId,
    PlatformType.SALESFORCE,
    ctx,
  )

  return await axios.get(
    `${connection.instance_url}/services/data/${apiVersion}/query?q=${query}`,
    {
      headers: {
        Authorization: connection.access_token,
      },
    },
  )
}

export async function getSalesforceContacts(ctx: NangoContext) {
  const query =
    'SELECT+Id,FirstName,LastName,Email,Phone,MobilePhone,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,AccountId,Title,Department,Birthdate,Description,CreatedDate,LastModifiedDate+FROM+Contact'
  const contacts: SalesforceContactPublishData[] = (await getFromQuery(ctx, query)).data.records

  return contacts
}

export async function getSalesforceUsers(ctx: NangoContext) {
  const query = 'SELECT+Id,Name,Email+FROM+User'
  const contacts: SalesforceUserPublishData[] = (await getFromQuery(ctx, query)).data.records

  return contacts
}

export async function getSalesforceOrganization(ctx: NangoContext) {
  const connection = await getNangoConnection(
    ctx.serviceSettings.nangoId,
    PlatformType.SALESFORCE,
    ctx,
  )

  return (
    await axios.get(`${connection.instance_url}/services/data/${apiVersion}/connect/organization`, {
      headers: {
        Authorization: connection.access_token,
      },
    })
  ).data
}

export async function getSalesforceAccounts(ctx: NangoContext) {
  const query = 'SELECT+Id,Name,Site,Website,ShippingAddress,BillingAddress,Industry+FROM+Account'
  const accounts: SalesforceOrganizationPublishData[] = (await getFromQuery(ctx, query)).data
    .records

  return accounts
}

export async function updateContact(ctx: NangoContext, contact: SalesforceContactPublishData) {
  const connection = await getNangoConnection(
    ctx.serviceSettings.nangoId,
    PlatformType.SALESFORCE,
    ctx,
  )

  await axios.patch(
    `${connection.instance_url}/services/data/${apiVersion}/sobjects/Contact/${contact.Id}`,
    {
      Email: contact.Email,
      Title: contact.Title,
    },
    {
      headers: {
        Authorization: connection.access_token,
      },
    },
  )
}

export async function updateCompany(ctx: NangoContext, account: SalesforceOrganizationPublishData) {
  const connection = await getNangoConnection(
    ctx.serviceSettings.nangoId,
    PlatformType.SALESFORCE,
    ctx,
  )

  await axios.patch(
    `${connection.instance_url}/services/data/${apiVersion}/sobjects/Account/${account.Id}`,
    {
      Industry: account.Industry,
      Name: account.Name,
      Website: account.Website,
    },
    {
      headers: {
        Authorization: connection.access_token,
      },
    },
  )
}
