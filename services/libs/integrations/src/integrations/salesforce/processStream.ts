// processStream.ts content
import { ProcessStreamHandler } from '../../types'
import { ISalesforcePublishData, SalesforceStreamType } from './types'
import { getSalesforceAccounts, getSalesforceContacts, getSalesforceOrganization } from './api'

const processContacts: ProcessStreamHandler = async (ctx) => {
  const contacts = await getSalesforceContacts(ctx)
  for (const contact of contacts) {
    await ctx.processData<ISalesforcePublishData>({
      type: SalesforceStreamType.CONTACTS,
      entity: contact,
    })
  }
}

const processOrganizations: ProcessStreamHandler = async (ctx) => {
  const accounts = await getSalesforceAccounts(ctx)
  const salesforceOrganization = await getSalesforceOrganization(ctx)
  for (const account of accounts) {
    account.OrganizationId = salesforceOrganization.orgId
    await ctx.processData<ISalesforcePublishData>({
      type: SalesforceStreamType.ORGANIZATIONS,
      entity: account,
    })
  }
}

const processRoot: ProcessStreamHandler = async (ctx) => {
  const salesforceOrganization = await getSalesforceOrganization(ctx)
  await ctx.processData<ISalesforcePublishData>({
    type: SalesforceStreamType.ROOT,
    entity: salesforceOrganization,
  })
}

const handler: ProcessStreamHandler = async (ctx) => {
  switch (ctx.stream.identifier) {
    case SalesforceStreamType.ORGANIZATIONS:
      await processOrganizations(ctx)
      break
    case SalesforceStreamType.CONTACTS:
      await processContacts(ctx)
      break
    case SalesforceStreamType.ROOT:
      await processRoot(ctx)
      break
    default:
      await ctx.abortRunWithError(`Unknown stream type: ${ctx.stream.identifier}`)
      break
  }
}

export default handler
