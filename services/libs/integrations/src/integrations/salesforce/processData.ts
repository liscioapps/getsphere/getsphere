// processData.ts content
import { ProcessDataHandler } from '../../types'
import {
  IMemberData,
  IOrganization,
  IntegrationResultType,
  MemberAttributeName,
  OrganizationAttributeName,
  OrganizationSource,
  PlatformType,
} from '@crowd/types'
import {
  ISalesforcePublishData,
  SalesforceContactPublishData,
  SalesforceOrganizationPublishData,
  SalesforceRootPublishData,
  SalesforceStreamType,
} from './types'

const processContact = async (ctx) => {
  const data = ctx.data.entity as SalesforceContactPublishData
  const member: IMemberData = {
    identities: [
      {
        platform: PlatformType.SALESFORCE,
        sourceId: data.Id,
        username: data.Email ?? data.AccountId,
      },
    ],
    emails: [data.Email],
    displayName: `${data.FirstName} ${data.LastName}`,
    attributes: {
      [MemberAttributeName.SOURCE_ID]: {
        [PlatformType.SALESFORCE]: data.Id,
      },
      [MemberAttributeName.NAME]: {
        [PlatformType.SALESFORCE]: `${data.FirstName} ${data.LastName}`,
      },
      [MemberAttributeName.BIO]: {
        [PlatformType.SALESFORCE]: data.Description,
      },
      [MemberAttributeName.JOB_TITLE]: {
        [PlatformType.SALESFORCE]: data.Title,
      },
    },
  }

  await ctx.publishCustom(member, IntegrationResultType.MEMBER_ENRICH)
}

const processOrganization = async (ctx) => {
  const data = ctx.data.entity as SalesforceOrganizationPublishData
  const organization: IOrganization = {
    identities: [
      {
        name: data.Name,
        platform: PlatformType.SALESFORCE,
        sourceId: data.Id,
        url: data.Website,
      },
    ],
    displayName: data.Name,
    address: {
      country: data.ShippingAddress?.Country,
      region: data.ShippingAddress?.State,
      continent: '',
      street_address: data.ShippingAddress?.Street,
      postal_code: data.ShippingAddress?.PostalCode,
      address_line_2: '',
      locality: data.ShippingAddress?.City,
      metro: '',
      name: data.Name,
    },
    industry: data.Industry,
    website: data.Website,
    attributes: {
      [OrganizationAttributeName.SOURCE_ID]: {
        [PlatformType.SALESFORCE]: data.Id,
      },
      [OrganizationAttributeName.URL]: {
        [PlatformType.SALESFORCE]: data.Website,
      },
    },
    source: OrganizationSource.SALESFORCE,
  }

  await ctx.publishCustom(organization, IntegrationResultType.ORGANIZATION_ENRICH)
}
const processRoot = async (ctx) => {
  const data = ctx.data.entity as SalesforceRootPublishData
  await ctx.updateIntegrationIdentifier(data.orgId)
}

const handler: ProcessDataHandler = async (ctx) => {
  const data = ctx.data as ISalesforcePublishData
  switch (data.type) {
    case SalesforceStreamType.CONTACTS:
      await processContact(ctx)
      break
    case SalesforceStreamType.ORGANIZATIONS:
      await processOrganization(ctx)
      break
    case SalesforceStreamType.ROOT:
      await processRoot(ctx)
      break
  }
}

export default handler
