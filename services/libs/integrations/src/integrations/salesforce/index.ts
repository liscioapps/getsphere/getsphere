// index.ts content
import { IIntegrationDescriptor } from '../../types'
import generateStreams from './generateStreams'
import { SALESFORCE_MEMBER_ATTRIBUTES } from './memberAttributes'
import processStream from './processStream'
import processData from './processData'
import processWebhookStream from './processWebhookStream'
import startSyncRemote from './startSyncRemote'
import processSyncRemote from './processSyncRemote'
import { PlatformType } from '@crowd/types'

const descriptor: IIntegrationDescriptor = {
  type: PlatformType.SALESFORCE,
  memberAttributes: SALESFORCE_MEMBER_ATTRIBUTES,
  generateStreams,
  processStream,
  processData,
  processWebhookStream,
  startSyncRemote,
  processSyncRemote,
}

export default descriptor
