// types.ts content

export enum SalesforceStreamType {
  CONTACTS = 'contacts',
  ORGANIZATIONS = 'organizations',
  ROOT = 'root',
}

export interface ISalesforceIntegrationSettings {
  enabledFor: SalesforceEntity[]
}

export interface ISalesforcePlatformSettings {
  token: string
  token2: string
}

export enum SalesforceEntity {
  MEMBERS = 'contacts',
  ORGANIZATIONS = 'organizations',
  ROOT = 'root',
}

export enum SalesforceDataType {
  CONTACTS = 'contacts',
}

export interface ISalesforcePublishData {
  type: SalesforceStreamType
  entity:
    | SalesforceContactPublishData
    | SalesforceOrganizationPublishData
    | SalesforceUserPublishData
    | SalesforceRootPublishData
}

export interface SalesforceContactPublishData {
  Id: string
  FirstName?: string
  LastName?: string
  Email?: string
  Description?: string
  Title?: string
  Department?: string
  AccountId?: string
}

export interface SalesforceUserPublishData {
  Id: string
  Email: string
  Name: string
}

export interface SalesforceOrganizationPublishData {
  Id?: string
  Name?: string
  Site?: string
  Website?: string
  ShippingAddress?: SalesforceAccountAddressEntity
  BillingAddress?: SalesforceAccountAddressEntity
  Industry?: string
  OrganizationId?: string
}

export interface SalesforceAccountAddressEntity {
  City: string
  Country: string
  GeocodeAccuracy: string
  Latitude: string
  Longitude: string
  PostalCode: string
  State: string
  Street: string
}

export interface SalesforceRootPublishData {
  orgId: string
}

export interface ISalesforceWebhookPayload {
  data: ISalesforcePublishData
}
