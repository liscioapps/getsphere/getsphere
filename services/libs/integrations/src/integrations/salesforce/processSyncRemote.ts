import { Entity, IMember, IOrganization } from '@crowd/types'
import {
  IBatchIntegrationOperationResult,
  IIntegrationProcessRemoteSyncContext,
  ProcessIntegrationSyncHandler,
} from '../../types'
import {
  mapMemberToSalesforce,
  mapOrganizationToSalesforce,
  salesforceCreateCompany,
  salesforceCreateMember,
} from './mapping'

type ProcessEntityHandler<T> = (
  toCreate: T[],
  toUpdate: T[],
  ctx: IIntegrationProcessRemoteSyncContext,
) => Promise<IBatchIntegrationOperationResult>

const processMembers: ProcessEntityHandler<IMember> = async (toCreate, toUpdate, ctx) => {
  const result: IBatchIntegrationOperationResult = { created: [], updated: [] }
  if (toCreate.length) {
    const contacts = toCreate.map(mapMemberToSalesforce)
    await salesforceCreateMember(ctx, contacts)
    result.created = toCreate.map((c) => ({
      lastSyncedPayload: c,
      memberId: c.id,
      sourceId: c.attributes?.sourceId?.salesforce,
    }))
  }
  if (toUpdate.length) {
    const contacts = toUpdate.map(mapMemberToSalesforce).map((contact, i) => ({
      ...contact,
    }))
    await salesforceCreateMember(ctx, contacts)
    result.updated = toUpdate.map((c) => ({
      lastSyncedPayload: c,
      memberId: c.id,
      sourceId: c.attributes?.sourceId?.salesforce,
    }))
  }
  return result
}

const processOrganizations: ProcessEntityHandler<IOrganization> = async (
  toCreate,
  toUpdate,
  ctx,
) => {
  const result: IBatchIntegrationOperationResult = { created: [], updated: [] }
  if (toCreate.length) {
    const companies = toCreate.map(mapOrganizationToSalesforce)
    await salesforceCreateCompany(ctx, companies)
    result.created = toCreate.map((c) => ({
      lastSyncedPayload: c,
      organizationId: c.id,
      sourceId: c.attributes?.sourceId?.salesforce,
    }))
  }
  if (toUpdate.length) {
    const companies = toUpdate.map(mapOrganizationToSalesforce).map((company, i) => ({
      ...company,
    }))
    await salesforceCreateCompany(ctx, companies)
    result.updated = toCreate.map((c) => ({
      lastSyncedPayload: c,
      organizationId: c.id,
      sourceId: c.attributes?.sourceId?.salesforce,
    }))
  }
  return result
}

const handler: ProcessIntegrationSyncHandler = async (toCreate, toUpdate, entity, ctx) => {
  switch (entity) {
    case Entity.MEMBERS:
      return await processMembers(toCreate as IMember[], toUpdate as IMember[], ctx)
    case Entity.ORGANIZATIONS:
      return await processOrganizations(
        toCreate as IOrganization[],
        toUpdate as IOrganization[],
        ctx,
      )
    default:
      throw new Error(`Unsupported entity: ${entity}`)
  }
}

export default handler
