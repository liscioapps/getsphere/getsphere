import { StartIntegrationSyncHandler } from '../../types'
import { ISalesforceIntegrationSettings, SalesforceEntity } from './types'

const handler: StartIntegrationSyncHandler = async (ctx) => {
  const settings = ctx.integration.settings as ISalesforceIntegrationSettings

  if (settings.enabledFor.includes(SalesforceEntity.MEMBERS)) {
    await ctx.integrationSyncWorkerEmitter.triggerSyncMarkedMembers(
      ctx.tenantId,
      ctx.integration.id,
    )
  }

  if (settings.enabledFor.includes(SalesforceEntity.ORGANIZATIONS)) {
    await ctx.integrationSyncWorkerEmitter.triggerSyncMarkedOrganizations(
      ctx.tenantId,
      ctx.integration.id,
    )
  }
}

export default handler
