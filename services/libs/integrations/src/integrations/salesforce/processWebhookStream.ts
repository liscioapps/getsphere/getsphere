import { ProcessWebhookStreamHandler } from '../../types'
import { ISalesforcePublishData, ISalesforceWebhookPayload, SalesforceStreamType } from './types'

const handler: ProcessWebhookStreamHandler = async (ctx) => {
  const payload = ctx.stream.data as ISalesforceWebhookPayload

  switch (payload.data.type) {
    case SalesforceStreamType.CONTACTS: {
      await ctx.processData<ISalesforcePublishData>({
        type: SalesforceStreamType.CONTACTS,
        entity: payload.data.entity,
      })
      break
    }
    case SalesforceStreamType.ORGANIZATIONS: {
      await ctx.processData<ISalesforcePublishData>({
        type: SalesforceStreamType.ORGANIZATIONS,
        entity: payload.data.entity,
      })
      break
    }

    default: {
      ctx.log.error(`Unknown salesforce websocket event: ${payload.data.type}!`)
      throw new Error(`Unknown salesforce websocket event: ${payload.data.type}!`)
    }
  }
}

export default handler
