import { IMember, IOrganization } from '@crowd/types'
import { SalesforceContactPublishData, SalesforceOrganizationPublishData } from './types'
import { updateCompany, updateContact } from './api'
import { NangoContext } from '../../types'

export function mapMemberToSalesforce(member: IMember): SalesforceContactPublishData {
  return {
    Id: member.attributes?.sourceId?.salesforce,
    Department: member.attributes?.jobTitle?.salesforce,
    Description: member.attributes?.bio?.salesforce,
    Email: member.emails[0],
    Title: member.attributes?.jobTitle?.salesforce,
  }
}

export async function salesforceCreateMember(
  ctx: NangoContext,
  contacts: SalesforceContactPublishData[],
) {
  for (const contact of contacts) {
    await updateContact(ctx, contact)
  }
}

export function mapOrganizationToSalesforce(
  organization: IOrganization,
): SalesforceOrganizationPublishData {
  return {
    Id: organization.attributes?.sourceId?.salesforce,
    Industry: organization.industry,
    Name: organization.displayName,
    Website: organization.website,
  }
}

export async function salesforceCreateCompany(
  ctx: NangoContext,
  companies: SalesforceOrganizationPublishData[],
) {
  for (const company of companies) {
    await updateCompany(ctx, company)
  }
}
