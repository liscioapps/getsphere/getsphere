import { getServiceLogger } from '@crowd/logging'
import * as Sentry from '@sentry/node'
import { nodeProfilingIntegration } from '@sentry/profiling-node'
import { ITelemetryProvider } from './types'
import { prefixName } from './utils'

const logger = getServiceLogger()
const dsn = process.env['CROWD_SENTRY_DSN']
if (dsn) {
  Sentry.init({
    dsn: dsn,
    integrations: [nodeProfilingIntegration()],
    tracesSampleRate: 1.0,
    profilesSampleRate: 1.0,
  })
  logger.info('Sentry enabled')
} else {
  logger.info('Sentry disabled')
}

const telemetry: ITelemetryProvider = {
  gauge: (name: string, value: number, tags?: Record<string, string | number>) => {
    Sentry.metrics.gauge(prefixName(name), value, tags)
  },
  distribution: (name: string, value: number, tags?: Record<string, string | number>) => {
    Sentry.metrics.distribution(prefixName(name), value, tags)
  },
  flush: () => {
    Sentry.flush()
  },
  increment: (name: string, value: number, tags?: Record<string, string | number>) => {
    Sentry.metrics.increment(prefixName(name), value, tags)
  },
  decrement: (name: string, value: number, tags?: Record<string, string | number>) => {
    Sentry.metrics.increment(prefixName(name), -value, tags)
  },
}

export default telemetry
