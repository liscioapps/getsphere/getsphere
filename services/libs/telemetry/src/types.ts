export interface ITelemetryProvider {
  gauge: (name: string, value: number, tags?: Record<string, string | number>) => void
  distribution: (name: string, value: number, tags?: Record<string, string | number>) => void
  flush: () => void
  increment: (name: string, value: number, tags?: Record<string, string | number>) => void
  decrement: (name: string, value: number, tags?: Record<string, string | number>) => void
}
