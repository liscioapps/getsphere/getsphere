import emptyTelemetry from './empty'
import { ITelemetryProvider } from './types'

let provider: ITelemetryProvider = emptyTelemetry

if (process.env['CROWD_DATADOG_ENABLED'] === 'true') {
  provider = require('./datadog').default
} else if (process.env['CROWD_SENTRY_DSN']) {
  provider = require('./sentry').default
}

const telemetry = {
  gauge: (name: string, value: number, tags?: Record<string, string | number>) => {
    provider.gauge(name, value, tags)
  },
  distribution: (name: string, value: number, tags?: Record<string, string | number>) => {
    provider.distribution(name, value, tags)
  },
  flush: () => {
    provider.flush()
  },
  increment: (name: string, value: number, tags?: Record<string, string | number>) => {
    provider.increment(name, value, tags)
  },
  decrement: (name: string, value: number, tags?: Record<string, string | number>) => {
    provider.decrement(name, value, tags)
  },
  timer: (name: string, tags?: Record<string, string | number>) => {
    const start = process.hrtime()
    return {
      stop: (extraTags?: Record<string, string | number>) => {
        const diff = process.hrtime(start)
        const duration = diff[0] * 1e3 + diff[1] * 1e-6
        telemetry.distribution(name, duration, {
          ...tags,
          ...extraTags,
        })
      },
    }
  },
  measure: async (
    name: string,
    fn: () => Promise<void>,
    tags?: Record<string, string | number>,
  ) => {
    const timer = telemetry.timer(`${name}`, tags)
    try {
      const result = await fn()
      timer.stop({ success: 'true' })
      return result
    } catch (error) {
      timer.stop({ success: 'false' })
      throw error
    }
  },
}

function normalizeUrl(url: string) {
  let result

  // replace uuids with placeholders
  result = url.replace(/[a-f0-9]{8}-([a-f0-9]{4}-){3}[a-f0-9]{12}/g, ':id')
  // remove query string
  result = result.replace(/\?.*$/, '')

  return result
}

export function telemetryExpressMiddleware(name) {
  return (req, res, next) => {
    const timer = telemetry.timer(name, {
      method: req.method,
      url: normalizeUrl(req.url),
    })
    res.on('finish', () => {
      timer.stop({
        status: res.statusCode,
      })
    })
    next()
  }
}

export default telemetry
