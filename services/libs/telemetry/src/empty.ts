import { ITelemetryProvider } from './types'

export const empty = () => {} // eslint-disable-line @typescript-eslint/no-empty-function

const telemetry: ITelemetryProvider = {
  gauge: empty,
  distribution: empty,
  flush: empty,
  increment: empty,
  decrement: empty,
}

export default telemetry
