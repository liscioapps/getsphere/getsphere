import { getServiceLogger } from '@crowd/logging'
import ddTrace from 'dd-trace'
import { ITelemetryProvider } from './types'
import { empty } from './empty'
import { prefixName } from './utils'

const logger = getServiceLogger()

function init() {
  const enabled = process.env['CROWD_DATADOG_ENABLED'] === 'true'
  if (!enabled) {
    logger.info('datadog disabled')
    return {
      dogstatsd: {
        increment: empty,
        decrement: empty,
        gauge: empty,
        distribution: empty,
        flush: empty,
      },
    }
  }

  const hostname = process.env['KUBE_HOST_IP'] || 'datadog-agent'
  logger.info('datadog enabled on', hostname)
  return ddTrace.init({
    hostname,
    startupLogs: true,
    logger: {
      debug: logger.info.bind(logger),
      info: logger.info.bind(logger),
      warn: logger.warn.bind(logger),
      error: logger.error.bind(logger),
    },
    dogstatsd: {
      hostname,
    },
    profiling: false,
    ingestion: {
      sampleRate: 0,
    },
  })
}

const datadog = init()


const telemetry: ITelemetryProvider = {
  gauge: (name: string, value: number, tags?: Record<string, string | number>) => {
    datadog.dogstatsd.gauge(prefixName(name), value, tags)
  },
  distribution: (name: string, value: number, tags?: Record<string, string | number>) => {
    datadog.dogstatsd.distribution(prefixName(name), value, tags)
  },
  flush: () => {
    datadog.dogstatsd.flush()
  },
  increment: (name: string, value: number, tags?: Record<string, string | number>) => {
    datadog.dogstatsd.increment(prefixName(name), value, tags)
  },
  decrement: (name: string, value: number, tags?: Record<string, string | number>) => {
    datadog.dogstatsd.decrement(prefixName(name), value, tags)
  },
}


export default telemetry
