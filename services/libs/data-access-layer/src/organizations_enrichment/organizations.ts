import { DbConnOrTx } from '@crowd/database'
import { TenantPlans } from '@crowd/types'
import { OrganizationData } from './types'
import { ENRICH_COLUMNS } from './caches'
import { checkUpdateRowCount } from '../utils'

export const getOrganizationsToSync = (
  conn: DbConnOrTx,
  { offset = 0, limit = 20 }: { offset: number; limit: number },
) => {
  return conn.any<{
    cacheId: string
    id: string
  }>(
    `
    select distinct oc.id as "cacheId", o.id
from "organizationCaches" oc
         inner join "organizationCacheLinks" ocl on oc.id = ocl."organizationCacheId"
         inner join organizations o on ocl."organizationId" = o.id
         inner join tenants t on t.id = o."tenantId" and t.plan in ($(plans:csv))
where oc."lastEnrichedAt" is not null and (oc."lastEnrichedAt" > o."lastEnrichedAt" or o."lastEnrichedAt" is null)
limit ${limit} offset ${offset}
    `,
    {
      plans: [TenantPlans.Business, TenantPlans.Enterprise, TenantPlans.Basic],
    },
  )
}

export const getOrganization = async (conn: DbConnOrTx, id: string) => {
  return await conn.oneOrNone<OrganizationData>(
    `
    with identities as (select oi."organizationId",
                              json_agg(json_build_object(
                                      'platform', oi.platform,
                                      'name', oi.name,
                                      'url', oi.url
                                        )) as "identities"
                        from "organizationIdentities" oi
                        where oi."organizationId" = $(id)
                        group by oi."organizationId")
    select o.id,
          o.description,
          o.emails,
          o."phoneNumbers",
          o.logo,
          o.twitter,
          o.linkedin,
          o.github,
          o.crunchbase,
          o.employees,
          o."revenueRange",
          o."tenantId",
          o.location,
          o.website,
          o."geoLocation",
          o.size,
          o.headline,
          o.address,
          o.industry,
          o.founded,
          o."displayName",
          o."allSubsidiaries",
          o."alternativeDomains",
          o."ultimateParent",
          o."weakIdentities",
          o."manuallyChangedFields",
          coalesce(i.identities, json_build_array()) as identities
    from organizations o
            left join identities i on o.id = i."organizationId"
    where o.id = $(id) and o."deletedAt" is null;
    `,
    {
      id,
    },
  )
}

export const updateOrganization = async (
  conn: DbConnOrTx,
  organization: OrganizationData,
  data: Partial<OrganizationData>,
) => {
  if (data.identities) {
    for (const identity of data.identities) {
      const existingIdentity = organization.identities.find(
        (i) => i.platform === identity.platform && i.name === identity.name,
      )
      if (existingIdentity) {
        if (identity.url) {
          await conn.result(
            `
            update "organizationIdentities"
            set url = $(url)
            where "tenantId"=$(tenantId) and "organizationId" = $(organizationId) and platform = $(platform) and name = $(name)
            `,
            {
              tenantId: organization.tenantId,
              organizationId: organization.id,
              platform: identity.platform,
              name: identity.name,
              url: identity.url,
            },
          )
        }
      } else {
        await conn.result(
          `
          insert into "organizationIdentities" ("organizationId", platform, name, url, "tenantId")
          values ($(organizationId), $(platform), $(name), $(url), $(tenantId))
          `,
          {
            organizationId: organization.id,
            platform: identity.platform,
            name: identity.name,
            url: identity.url,
            tenantId: organization.tenantId,
          },
        )
      }
    }
  }
  const updateData: any = {}
  for (const key of ENRICH_COLUMNS) {
    if (data[key]) {
      updateData[key] = data[key]
    }
  }

  updateData.lastEnrichedAt = new Date()

  const keys = Object.keys(updateData)
  const params: any = {
    id: organization.id,
  }

  const sets: string[] = []
  for (const key of keys) {
    sets.push(`"${key}" = $(${key})`)
    params[key] = updateData[key]
  }

  const query = `
  update "organizations" set
  ${sets.join(', \n')}
  where id = $(id);
`

  const result = await conn.result(query, params)
  checkUpdateRowCount(result.rowCount, 1)
}
