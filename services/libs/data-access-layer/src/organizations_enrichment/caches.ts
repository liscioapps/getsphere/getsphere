import { DbConnOrTx } from '@crowd/database'
import { TenantPlans } from '@crowd/types'
import { OrganizationCacheData, OrganizationCacheListData } from './types'
import { checkUpdateRowCount } from '../utils'

export const getEnrichableOrganizationCaches = (
  conn: DbConnOrTx,
  { offset = 0, limit = 20 }: { offset: number; limit: number },
) => {
  return conn.any<OrganizationCacheListData>(
    `
    select distinct oc.id
from "organizationCaches" oc
         inner join "organizationCacheLinks" ocl on oc.id = ocl."organizationCacheId"
         inner join organizations o on ocl."organizationId" = o.id
         inner join tenants t on t.id = o."tenantId" and t.plan in ($(plans:csv))
where oc."lastEnrichedAt" is null
   or oc."lastEnrichedAt" < now() - interval '1 year'
limit ${limit} offset ${offset}
    `,
    {
      plans: [TenantPlans.Business, TenantPlans.Enterprise, TenantPlans.Basic],
    },
  )
}

export const getOrganizationCache = (conn: DbConnOrTx, id: string) => {
  return conn.oneOrNone<OrganizationCacheData>(
    `
    with identities as (select oi.id,
                           json_agg(json_build_object(
                                   'name', oi.name,
                                   'website', oi.website
                                    )) as identities
                    from "organizationCacheIdentities" oi
                    group by oi.id)
select oc.id,
       oc.description,
       oc.emails,
       oc."phoneNumbers",
       oc.logo,
       oc.tags,
       oc.twitter,
       oc.linkedin,
       oc.employees,
       oc."revenueRange",
       oc.location,
       oc."geoLocation",
       oc.size,
       oc.headline,
       oc.address,
       oc.founded,
       oc.industry,
       oc."allSubsidiaries",
       oc."alternativeDomains",
       oc."ultimateParent",
       i.identities
from "organizationCaches" oc
         inner join identities i on oc.id = i.id
where oc.id = $(id)
    `,
    { id },
  )
}

export const ENRICH_COLUMNS = [
  'description',
  'emails',
  'phoneNumbers',
  'logo',
  'twitter',
  'linkedin',
  'crunchbase',
  'employees',
  'revenueRange',
  'location',
  'github',
  'size',
  'headline',
  'address',
  'industry',
  'allSubsidiaries',
  'alternativeDomains',
  'ultimateParent',
  'url',
  'geoLocation',
]

export const markOrganizationCacheAsEnriched = async (conn: DbConnOrTx, id: string) => {
  await conn.none(
    `
    update "organizationCaches"
    set "lastEnrichedAt" = now()
    where id = $(id)
    `,
    { id },
  )
}

export const updateOrganizationCache = async (
  conn: DbConnOrTx,
  cache: OrganizationCacheData,
  data: any,
) => {
  if (data.name && !cache.identities.some((i) => i.name === data.name)) {
    await conn.none(
      `insert into "organizationCacheIdentities"(id, name, website) values($(id), $(name), $(website))`,
      {
        id: cache.id,
        name: data.name,
        website: data.domainName,
      },
    )
  }

  const updateData: any = {}
  for (const key of ENRICH_COLUMNS) {
    if (data[key] !== undefined) {
      updateData[key] = data[key]
    }
  }

  updateData['lastEnrichedAt'] = new Date()

  const keys = Object.keys(updateData)
  const params: any = {
    id: cache.id,
  }

  const sets: string[] = []
  for (const key of keys) {
    sets.push(`"${key}" = $(${key})`)
    params[key] = updateData[key]
  }

  const query = `
  update "organizationCaches" set
  ${sets.join(', \n')}
  where id = $(id);
`
  const result = await conn.result(query, params)
  checkUpdateRowCount(result.rowCount, 1)
}
