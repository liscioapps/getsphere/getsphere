export type OrganizationCacheListData = {
  id: string
}

export type OrganizationSocialIdentity = {
  handle: string
  url?: string
}

export interface OrganizationCacheIdentity {
  name: string
  website: string | null
}

export type OrganizationCacheData = {
  id: string
  description: string | null
  emails: string[] | null
  phoneNumbers: string[] | null
  logo: string | null
  twitter: OrganizationSocialIdentity | null
  linkedin: OrganizationSocialIdentity | null
  employees: number | null
  revenueRange: unknown | null
  location: string | null
  geoLocation: string | null
  size: string | null
  headline: string | null
  address: unknown | null
  industry: string | null
  founded: number | null
  allSubsidiaries: string[] | null
  alternativeDomains: string[] | null
  ultimateParent: string | null
  identities: OrganizationCacheIdentity[]
}

export type OrganizationIdentity = {
  platform: string
  name: string
  url: string | null
}

export type OrganizationData = {
  id: string
  tenantId: string
  description: string | null
  emails: string[] | null
  phoneNumbers: string[] | null
  logo: string | null
  tags: string[] | null
  twitter: OrganizationSocialIdentity | null
  linkedin: OrganizationSocialIdentity | null
  crunchbase: OrganizationSocialIdentity | null
  github: OrganizationSocialIdentity | null
  employees: number | null
  revenueRange: unknown | null
  location: string | null
  website: string | null
  geoLocation: string | null
  size: string | null
  address: unknown | null
  industry: string | null
  founded: number | null
  displayName: string | null
  allSubsidiaries: string[] | null
  alternativeDomains: string[] | null
  ultimateParent: string | null
  manuallyChangedFields: string[]
  identities: OrganizationIdentity[]
  weakIdentities: OrganizationIdentity[]
}
